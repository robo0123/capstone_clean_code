var infoIcon = function (cell, formatterParams) {
    return "<i class='fas fa-info-circle'></i>";
};

var deleteIcon = function (cell, formatterParams) {
    return "<i class='fas fa-trash-alt'></i>";
};

var percentIcon = function (cell, formatterParams) {
    return "<i class='fas fa-percentage'></i>";
};

var penIcon = function (cell, formatterParams) {
    return "<i class='fas fa-pen-alt'></i>";
};

var addIcon = function (cell, formatterParams) {
    return "<i class='fas fa-plus'></i>";
};

var payIcon = function (cell, formatterParams) {
    return "<i class='fas fa-hand-holding-usd'></i>";
};

var snowflake = function (cell, formatterParams) {
    return "<i class='far fa-snowflake'></i>";
};

function addCSSonForm() {
    $('label').addClass("has-text-weight-bold");
    $('.errorlist').addClass("has-text-danger");
}

function popupSA(head, body) {

    Swal.fire({
        title: head,
        text: body,
        showConfirmButton: false,
    }
    );

}

function popupSAwithFunction(head, body, extraFunction) {

    Swal.fire({
        title: head,
        text: body,
        showConfirmButton: false,
    }).then(() => {
        extraFunction();
    });
}

function popupModal(head, body) {
    $('#popup-modal-head').empty();
    $('#popup-modal-body').empty();

    $("#popup-modal-head").append(head);
    $("#popup-modal-body").append(body);

    $('#popup-modal').addClass("is-active");
}

function filterTable(input, table) {
    input.addEventListener("keyup", function () {
        var filters = [];
        var columns = table.getColumns();
        var search = input.value;

        columns.forEach(function (column) {
            filters.push({
                field: column.getField(),
                type: "like",
                value: search,
            });
        });
        table.setFilter([filters]);
    });
}

function refreshTable(table, load_url) {
    //table parameter is for tabulator table
    //load_url is ajax calls to get data
    $.ajax({
        'async': false,
        'type': "GET",
        'global': false,
        'dataType': 'json',
        'url': load_url,
        //'data': { 'request': "", 'target': 'arrange_url', 'method': 'method_target' },
        'success': function (data) {
            //tableData = data;
            table.setData(data);

        }
    });
}


function noFunction() {

}

function clearInput(form) {
    $('p > input').val('');
    $('p > textarea').val('');
}

function rebuildForm(form, formData, formToken) {
    form.html(formData);
    form.prepend(formToken);
    form.append('<div class="field mt-2">');
    form.append('<input class="button is-danger" type="reset" value="Reset">');
    form.append('<input class="button is-success" type="submit" value=Submit>');
    form.append('</div>');
    addCSSonForm();

}

function rebuildContactForm(url) {
    $.ajax({
        type: 'GET',
        url: url,
        dataType: "json",
        success: function (data) {
            rebuildForm(addContactForm, data.form, addContactFormTok);
        },
        error: function (data) {

        }
    });
}

function rebuildKnownSuppliersForm(url) {
    $.ajax({
        type: 'GET',
        url: url,
        dataType: "json",
        success: function (data) {
            rebuildForm(linkSupplierForm, data.form, linkSupplierFormTok);
        },
        error: function (data) {

        }
    });
}


/*==================================================================*/
//modal form functions
function openModal(button, modal) {
    button.click(function () {
        modal.addClass("is-active");
    });
}

function setCloseModal() {
    $(".modal-close").click(function () {
        $(this).closest('.modal').removeClass("is-active");
    });

    $(".modal-background").click(function () {
        $(this).closest('.modal').removeClass("is-active");
    });
}

function openModalClearForm(button, modal, formID) {
    button.click(function () {
        let csrf = $(formID + " input[name='csrfmiddlewaretoken']").clone();
        $(formID + " input[name='csrfmiddlewaretoken']").remove();
        modal.addClass("is-active");
        $(formID + " input").not($('.dt')).val('');
        $(formID + " textarea").val('');
        $(formID).prepend(csrf);
    });
}

/*==================================================================*/

function submitColorFormAndReloadTable(form, formToken, postURL, tableURL, table, success, error, extraFunction) {

    form.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: postURL,
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                if (data.devMessage == 'success') {
                    alert(success);
                    refreshTable(table, tableURL); //in my_helpers.js
                    form.trigger('reset');
                    extraFunction();
                } else {
                    alert(data.devMessage);
                }
            },
            error: function (data) {
            }
        });
    });

}

function submitFormAndReloadTable(form, formToken, postURL, tableURL, table, success, error, extraFunction) {

    form.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: postURL,
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                if (data.devMessage == 'success') {

                    refreshTable(table, tableURL); //in my_helpers.js
                    rebuildForm(form, data.form, formToken);
                    if (data.extraMessage != null) {
                        popupSA('', success + "\n" + data.extraMessage);
                    } else {
                        popupSA('', success);
                    }


                    extraFunction();
                } else {
                    rebuildForm(form, data.form, formToken);
                    console.log(data.form)
                    popupSA('', data.devMessage);
                }
            },
            error: function (xhr, status, error) {

            },
        });
    });

}

function submitFormAndReloadTableConditional(form, formToken, postURL, tableURL, table, success, error, extraFunction) {

    form.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: postURL,
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                if (data.devMessage == 'success') {

                    refreshTable(table, tableURL); //in my_helpers.js
                    rebuildForm(form, data.form, formToken);
                    if (data.extraMessage != null) {
                        extraFunction();
                        popupSA('', success + "\n" + data.extraMessage);
                    } else {
                        clearInput();
                        popupSA('', success);
                    }

                } else {
                    popupSA('', data.devMessage);
                    rebuildForm(form, data.form, formToken);
                }
            },
            error: function (xhr, status, error) {

            },
        });
    });

}

function submitFormAndReloadTableConditionalEF(form, formToken, postURL, tableURL, table, success, error, extraFunction, extraFunctionTwo) {

    form.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: postURL,
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                if (data.devMessage == 'success') {

                    refreshTable(table, tableURL); //in my_helpers.js
                    rebuildForm(form, data.form, formToken);
                    if (data.extraMessage != null) {
                        extraFunction();
                        extraFunctionTwo();
                        popupSA('', success + "\n" + data.extraMessage);
                    } else {
                        extraFunctionTwo();
                        popupSA('', success);
                    }

                } else {
                    popupSA('', data.devMessage);
                    rebuildForm(form, data.form, formToken);
                }
            },
            error: function (xhr, status, error) {

            },
        });
    });

}


function submitFormAndReloadTableElseSuccess(form, formToken, postURL, tableURL, table, success, error, extraFunction, extf2) {

    form.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: postURL,
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                if (data.devMessage == 'success') {

                    refreshTable(table, tableURL); //in my_helpers.js
                    rebuildForm(form, data.form, formToken);
                    if (data.extraMessage != null) {
                        alert(success + "\n" + data.extraMessage);
                        extf2();
                    } else {
                        alert(success);
                    }

                    extraFunction();
                } else {
                    alert(data.devMessage);
                    rebuildForm(form, data.form, formToken);
                }
            },
            error: function (xhr, status, error) {

            },
        });
    });

}


function deleteFromTable(form, formToken, tableURL, table, success, error, extraFunction) {

    form.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                if (data.devMessage == 'success') {
                    refreshTable(table, tableURL);
                    extraFunction();
                    popupSA('', success);
                } else {
                    form.trigger('reset');
                    popupSA('', data.devMessage);
                }
            },
            error: function (data) {

            }
        });
    });

}

function updateTable(form, formToken, tableURL, table, success, error, extraFunction) {

    form.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                if (data.devMessage == 'success') {
                    alert(success);
                    refreshTable(table, tableURL);
                    extraFunction();
                } else {
                    alert(data.devMessage);
                    rebuildForm(form, data.form, formToken);
                }
            },
            error: function (data) {

            }
        });
    });
}

function submitDelete(form, formToken, postURL, success, error, extraFunction) {

    form.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: postURL,
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                if (data.devMessage == 'success') {
                    popupSAwithFunction('', success, extraFunction);
                    //popupModal('',success);
                } else {
                    form.trigger('reset');
                    popupSA('', data.devMessage);
                    //popupModal('',data.devMessage);
                }
            },
            error: function (xhr, status, error) {

            },
        });
    });

}

function submitUpdate(form, formToken, postURL, success, error, extraFunction) {

    form.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: postURL,
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                if (data.devMessage == 'success') {
                    popupSAwithFunction('', success, extraFunction);
                    //popupModal('',success);
                } else {
                    rebuildForm(form, data.form, formToken);
                    popupSA('', data.devMessage);
                    //popupModal('',data.devMessage);
                }
            },
            error: function (xhr, status, error) {

            },
        });
    });

}

function linkItemToProj(form, formToken, postURL, success, error, extraFunction) {

    form.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: postURL,
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                if (data.devMessage == 'success') {
                    popupSAwithFunction('', success, extraFunction);
                    //popupModal('',success);
                }else{
                    popupSA('', data.devMessage)
                }
            },
            error: function (xhr, status, error) {

            },
        });
    });

}


function updateForm(form, formToken, formURL) {
    $.ajax({
        type: 'GET',
        url: formURL,
        //data: form.serialize(),
        dataType: "json",
        success: function (data) {
            if (data.error != null) {
                Swal.fire({
                    text: data.error
                });
            } else {
                rebuildForm(form, data.form, formToken);
            }

        },
        error: function (xhr, status, error) {

        },
    });

}

function displayText(title, text) {
    Swal.fire({
        title: title,
        text: text
    });
}

function moneyFormatter(number) {
    result = "₱ " + number.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
    return result;
}