from django.urls import path
from . import views

urlpatterns = [
    path('about/', views.about, name='about'),
    path('demo_registration/', views.createDemoUser, name='create_user_demo'),
    path('create_user/', views.createUser, name='create_user'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
    path('', views.home, name='home'),
]