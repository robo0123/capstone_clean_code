from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.contrib.auth.models import User, Group

from .forms import CreateUserForm, CreateUserFormDemo
from django.contrib.auth.decorators import login_required
from accounts.decorators import role_required
import logging

# Create your views here.
logger = logging.getLogger('django')

def createDemoUser(request):

    if request.user.is_authenticated:
        return redirect('home')

    form = CreateUserFormDemo()

    if request.method == 'POST':
        form = CreateUserFormDemo(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            groupName = form.cleaned_data.get('group')
            group = Group.objects.get(name=groupName)
            user = User.objects.get(username = username)
            user.groups.add(group)
            messages.success(request, 'Account was created for user: ' + username)
            return HttpResponseRedirect('')

    context = {'form': form} # put message here instead and print it using tags
    return render(request, 'accounts/demo_registration.html', context)


@role_required(allowed_roles=[])
@login_required(login_url='/login/')
def createUser(request):
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request, 'Account was created for user: ' + user)
            return HttpResponseRedirect('')

    context = {'form': form} # put message here instead and print it using tags
    return render(request, 'accounts/create_user.html', context)


def loginPage(request):
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        passcode = request.POST.get('passcode')
        ip = get_client_ip(request)

        if passcode == '8snK5Y6U576TNoyfUOCGI':
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                successLogin = 'Successful login from ip: ' + ip + ' username: ' + username
                logger.info(successLogin)
                return redirect('home')
            else:
                failedLogin =  'Failed login from ip: ' + ip + ' username: ' + username
                logger.info(failedLogin)
                messages.info(request, 'Username or Password is incorrect')
        else:
            messages.info(request, 'Wrong pass code. You need it to enter the demo.')
            user = None
            failedLogin =  'Failed login from ip: ' + ip + ' username: ' + username + ' - wrong passcode'
            logger.info(failedLogin)

        

    context = {}
    return render(request, 'accounts/login.html', context)

@login_required(login_url='/login/')
def logoutUser(request):
    logout(request)
    return redirect('login')

@login_required(login_url='/login/')
def home(request):
    return render(request, 'accounts/temp_home.html')


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

@login_required(login_url='/login/')
def about(request):
    return render(request, 'accounts/about.html')