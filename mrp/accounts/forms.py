from django.contrib.auth.models import User, Group
from django import forms
from django.contrib.auth.forms import UserCreationForm
# from django.forms import ModelForm


class CreateUserForm(UserCreationForm):

    first_name = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={'class': "input", 'type': 'text', 'placeholder': 'First name'}))
    last_name = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={'class': "input", 'type': 'text', 'placeholder': 'Last name'}))
    email = forms.EmailField(max_length=150, label='e-mail',
                             widget=forms.TextInput(
                                 attrs={'class': "input", 'type': 'email', 'placeholder': 'e-mail'}))
    password1 = forms.CharField(
        label='Password',
        widget=forms.PasswordInput(
            attrs={'class': "input", 'type': 'password', 'placeholder': 'Password'}))
    password2 = forms.CharField(
        label='Confirm Password',
        widget=forms.PasswordInput(
            attrs={'class': "input", 'type': 'password', 'placeholder': 'Confirm Password'}))

    def clean_email(self):
        if User.objects.filter(email=self.cleaned_data['email']).exists():
            raise forms.ValidationError(
                "The given e-mail is already registered.")
        return self.cleaned_data['email']

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username',
                  'email', 'password1', 'password2']

        widgets = {
            'username': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'username'}),



        }

class CreateUserFormDemo(UserCreationForm):

    first_name = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={'class': "input", 'type': 'text', 'placeholder': 'First name'}))
    last_name = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={'class': "input", 'type': 'text', 'placeholder': 'Last name'}))
    email = forms.EmailField(max_length=150, label='e-mail',
                             widget=forms.TextInput(
                                 attrs={'class': "input", 'type': 'email', 'placeholder': 'e-mail'}))
    password1 = forms.CharField(
        label='Password',
        widget=forms.PasswordInput(
            attrs={'class': "input", 'type': 'password', 'placeholder': 'Password'}))
    password2 = forms.CharField(
        label='Confirm Password',
        widget=forms.PasswordInput(
            attrs={'class': "input", 'type': 'password', 'placeholder': 'Confirm Password'}))
    group = forms.ModelChoiceField(queryset=Group.objects.all(),
                                   required=True)

    def clean_email(self):
        if User.objects.filter(email=self.cleaned_data['email']).exists():
            raise forms.ValidationError(
                "The given e-mail is already registered.")
        return self.cleaned_data['email']

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username',
                  'email', 'password1', 'password2', 'group']

        widgets = {
            'username': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'username'}),



        }