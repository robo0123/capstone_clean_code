from django.http import HttpResponse
from django.shortcuts import redirect

def role_required(allowed_roles=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):

            if request.user.is_superuser:
                return view_func(request, *args, **kwargs)

            for group in request.user.groups.all():
                if group.name in allowed_roles:
                    return view_func(request, *args, **kwargs)

            return redirect('home')
                    
        return wrapper_func
    return decorator