# Generated by Django 3.1.5 on 2021-01-12 03:48

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('inventory', '0003_auto_20201230_1241'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConstructionProject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('permit', models.CharField(max_length=140)),
                ('client_info', models.TextField()),
                ('address', models.CharField(max_length=280)),
                ('tags', models.TextField(blank=True)),
                ('remarks', models.TextField(blank=True, null=True)),
                ('frozen', models.BooleanField(default=False, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ItemTransactionTracker',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('constProject', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='construction_journal.constructionproject')),
                ('itemTrans', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='inventory.itemtransactions')),
            ],
        ),
        migrations.CreateModel(
            name='HistoricalItemTransactionTracker',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('quantity', models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('constProject', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='construction_journal.constructionproject')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('itemTrans', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='inventory.itemtransactions')),
            ],
            options={
                'verbose_name': 'historical item transaction tracker',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalConstructionProject',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('permit', models.CharField(max_length=140)),
                ('client_info', models.TextField()),
                ('address', models.CharField(max_length=280)),
                ('tags', models.TextField(blank=True)),
                ('remarks', models.TextField(blank=True, null=True)),
                ('frozen', models.BooleanField(default=False, null=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical construction project',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
    ]
