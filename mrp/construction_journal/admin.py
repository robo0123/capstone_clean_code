from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from .models import ConstructionProject, ItemTransactionTracker


# Register your models here.

class ROAHAdmin(SimpleHistoryAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        pass

    def delete_model(self, request, obj):
        pass

    def save_related(self, request, form, formsets, change):
        pass

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save': False,
            'show_save_and_continue': False,
            'show_delete': False
        })
        return super().render_change_form(request, context, add, change, form_url, obj)


class ConstructionProjectAdmin(ROAHAdmin):
    readonly_fields = ('job_order_num', 'client_info', 'address',
                       'tags', 'remarks', 'frozen', 'project')
    search_fields = ('job_order_num',)

admin.site.register(ConstructionProject, ConstructionProjectAdmin)

class ItemTrackerAdmin(ROAHAdmin):
    readonly_fields = ('constProject', 'itemTrans', 'quantity')
    search_fields = ('constProject', 'itemTrans', 'quantity')

admin.site.register(ItemTransactionTracker, ItemTrackerAdmin)