from django.apps import AppConfig


class ConstructionJournalConfig(AppConfig):
    name = 'construction_journal'
