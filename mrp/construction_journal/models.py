from django.db import models
from simple_history.models import HistoricalRecords
from inventory.models import ItemTransactions
from django.core.validators import MinValueValidator

# Create your models here.


class ConstructionProject(models.Model):
    job_order_num = models.CharField(max_length=140, blank=False, null=False)
    project = models.CharField(max_length=200, blank=False, null=False)
    client_info = models.TextField(blank=False, null=False)
    address = models.CharField(max_length=280, blank=False, null=False)
    tags = models.TextField(blank=True, null=False)
    remarks = models.TextField(blank=True, null=True)
    frozen = models.BooleanField(blank=False, null=True, default=False)
    history = HistoricalRecords()

    def __str__(self):
        return self.job_order_num


class ItemTransactionTracker(models.Model):
    constProject = models.ForeignKey(
        ConstructionProject, on_delete=models.RESTRICT, blank=False, null=False)
    itemTrans = models.ForeignKey(
        ItemTransactions, on_delete=models.RESTRICT, blank=False, null=False)
    quantity = models.PositiveIntegerField(
        blank=False, null=False, validators=[MinValueValidator(1)])
    history = HistoricalRecords()

    def __str__(self):
        return (self.constProject.job_order_num + ' - ' + self.itemTrans.item.name + ' - qty: ' + str(self.quantity))
