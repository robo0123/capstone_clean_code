from django.shortcuts import render
from django.shortcuts import redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse
import json
from .forms import ProjectForm
from .models import ConstructionProject, ItemTransactionTracker
from inventory.models import ItemAvailability, ItemTransactions, ItemInfo
from django.views.decorators.http import require_POST
from django.contrib.auth.hashers import check_password
from django.db.models import F, Sum, RestrictedError, FloatField, Count
from django.core.exceptions import ObjectDoesNotExist
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.db import transaction
from simple_history.utils import bulk_create_with_history, bulk_update_with_history
from accounts.decorators import role_required
# Create your views here.

@role_required(allowed_roles=['manager', 'inventory'])
def constructionPage(request):

    form = ProjectForm()
    context = {'form': form}

    if request.method == 'POST':
        form = ProjectForm(request.POST)
       
        if form.is_valid():
            project = form.save(commit=False)
            tags = cleanTags(form.cleaned_data['tags'])
            joinedTags = ", ".join(tags)
            project.tags = joinedTags
            # this is so that tags is not ruined and clean

            project.save()
            message = 'success'
            return JsonResponse({"form": form.as_p(), "devMessage": message})

        return JsonResponse({"form": form.as_p(), "devMessage": 'error'})

    return render(request, 'construction_journal/construction.html', context)


@role_required(allowed_roles=['manager', 'inventory'])
def loadProjectsAjax(request):
   
    allProjects = ConstructionProject.objects.all()
    jsonProjects = []
    for project in allProjects:
        aProject = {'pk': project.pk,
                    'job_order_num': project.job_order_num,
                    'project': project.project,
                    'client_info': project.client_info,
                    'address': project.address,
                    'tags': project.tags,
                    'remarks': project.remarks,
                    'frozen': project.frozen
                    }
        jsonProjects.append(aProject)

    jsonProjects = json.dumps(jsonProjects, indent=4)
    return HttpResponse(jsonProjects, content_type="application/json")


@role_required(allowed_roles=['manager', 'inventory'])
def viewMoreProjectInfo(request, pk):
    projectInfo = get_object_or_404(ConstructionProject, pk=pk)
    updateForm = ProjectForm(instance=projectInfo)

    context = {'updateForm': updateForm,
               'projectInfo': projectInfo,
               }
    return render(request, 'construction_journal/moreinfo.html', context)


@role_required(allowed_roles=['manager', ])
@require_POST
def freezeProject(request, id):

    if request.method == 'POST':
        password = request.POST.get("password", "")
        correct_password = check_password(password, request.user.password)

        if correct_password:
            project = None
            message = None
            
            try:
                project = ConstructionProject.objects.get(pk=id)
                if project.frozen is True:
                    message = 'Project already frozen.'
                elif project.frozen is False:
                    project.frozen = True
                    message = 'success'
    
            except ConstructionProject.DoesNotExist:
                message = 'Does Not Exist: The project does not exist.'


            return JsonResponse({'devMessage': message})
        else:
            return JsonResponse({'devMessage': 'Wrong Password'})


@role_required(allowed_roles=['manager', ])
@require_POST
def deleteProjectInfo(request, id):

    if request.method == 'POST':
        password = request.POST.get("password", "")
        correct_password = check_password(password, request.user.password)

        if correct_password:
            project = None
            message = None
            delete_success = False
            try:
                project = ConstructionProject.objects.get(pk=id)
                if project.frozen is True:
                    delete_success = False
                    message = 'Project is frozen. Project not deleted.'
                else:
                    project.delete()
                    delete_success = True
                    message = 'success'
            except RestrictedError:
                delete_success = False
                message = 'Restricted Error: A part of the system is using this Project. You must delete those first.'
            except ConstructionProject.DoesNotExist:
                delete_success = False
                message = 'Does Not Exist: The project does not exist.'

            return JsonResponse({'devMessage': message})
        else:
            return JsonResponse({'devMessage': 'Wrong Password'})


@role_required(allowed_roles=['manager', ])
@require_POST
def updateProjectInfo(request, id):
    try:
        if request.method == 'POST':
            instance = ConstructionProject.objects.get(pk=id)         
            form = ProjectForm(request.POST, instance=instance)
            project = None
            if instance.frozen is True:
                return JsonResponse({'devMessage': 'Project is frozen. Update failed.', 'form': form.as_p()})
          
            if form.is_valid():
                projectUpdate = form.save(commit=False)
                tags = cleanTags(form.cleaned_data['tags'])
                joinedTags = ", ".join(tags)
                projectUpdate.tags = joinedTags
                projectUpdate.save()

                project = ConstructionProject.objects.get(pk=id)
                return JsonResponse({'devMessage': 'success', 'form': form.as_p()})
        return JsonResponse({'devMessage': 'Error', 'form': form.as_p()})
    except ConstructionProject.DoesNotExist:
        form = ProjectForm()
        return JsonResponse({'devMessage': 'Does Not Exist: Construction Project does not exist', 'form': form.as_p()})


@role_required(allowed_roles=['manager', 'inventory'])
def loadItemsToProj(request, id):
    transactions = ItemTransactionTracker.objects.filter(constProject=id)
    aggreTrans = transactions.values(itemName=F('itemTrans__item__name')).order_by(
        'itemTrans__item__name').annotate(quantity=Sum('quantity'))
    items = ItemInfo.objects.all()
    jsonTransactions = []

    for obj in aggreTrans:
        trans = {
            'pk': items.get(name=obj['itemName']).pk,
            'itemName': obj['itemName'],
            'quantity': obj['quantity'],
            'parent': "true",
            'totalPrice': 0,
            '_children': []

        }
        jsonTransactions.append(trans)


    for transaction in transactions:
        tp = transaction.itemTrans.price_per_piece * transaction.quantity
        trans = {
            'pk': transaction.pk,
            'itemName': transaction.itemTrans.item.name,
            'supplier': transaction.itemTrans.supplied_by.name,
            'date': str(transaction.itemTrans.date),
            'quantity': transaction.quantity,
            'totalPrice': str(tp)
        }

        item_index = next((index for (index, d) in enumerate(
            jsonTransactions) if d["itemName"] == transaction.itemTrans.item.name), None)
        jsonTransactions[item_index]['totalPrice'] += tp
        jsonTransactions[item_index]['_children'].append(trans)

    for t in jsonTransactions:
        t['totalPrice'] = str(t['totalPrice'])

    jsonTransactions = json.dumps(jsonTransactions, indent=4)
    return HttpResponse(jsonTransactions, content_type="application/json")


@role_required(allowed_roles=['manager', 'inventory'])
@transaction.atomic
def addItemsToProj(request, pid, iid):
    try:
        if request.method == 'POST':

            item = ItemAvailability.objects.get(item=iid)
            project = ConstructionProject.objects.get(pk=pid)
            input_quantity = int(request.POST.get("quantity", ""))
            isAvailable = False
            batch = []
            batchUpdate = []
            transBatch = []

            if project.frozen is True:
                return JsonResponse({'devMessage': 'Project Frozen, Items not added'})


            # check if amount inputted is available
            if (input_quantity <= item.total_available and input_quantity > 0):
                item.total_available -= input_quantity
                item.total_used += input_quantity
                item.save()
                isAvailable = True

            # if available, marks the transactions that were used FIFO
            if isAvailable:
                itemTrans = ItemTransactions.objects.filter(
                    item=item.item, used__lt=F('quantity')).order_by('date')
                trackerList = ItemTransactionTracker.objects.filter(
                    constProject=project)

                for transaction in itemTrans.iterator():
                    unused = transaction.quantity - transaction.used
                    inTracker = None
                    try:
                        inTracker = trackerList.get(
                            constProject=project, itemTrans=transaction)
                    except ItemTransactionTracker.DoesNotExist:
                        inTracker = None

                    if (unused <= input_quantity):
                        input_quantity -= unused
                        transaction.used += unused

                        if inTracker is not None:
                            inTracker.quantity += unused
                            batchUpdate.append(inTracker)
                        else:
                            x = ItemTransactionTracker(
                                constProject=project, itemTrans=transaction, quantity=unused)
                            batch.append(x)

                        transBatch.append(transaction)

                        if input_quantity == 0:
                            break

                            # save(current.used) batch save later
                    elif (unused > input_quantity):
                        used = unused - (unused - input_quantity)
                        transaction.used += used
                        input_quantity = 0

                        if inTracker is not None:
                            inTracker.quantity += used
                            batchUpdate.append(inTracker)
                        else:
                            x = ItemTransactionTracker(
                                constProject=project, itemTrans=transaction, quantity=used)
                            batch.append(x)

                        transBatch.append(transaction)
                        break

                #ItemTransactionTracker.objects.bulk_create_with_history(batch)
                bulk_create_with_history(batch, ItemTransactionTracker)
                #ItemTransactionTracker.objects.bulk_update_with_history(
                #    batchUpdate, ['quantity'])
                bulk_update_with_history(batchUpdate, ItemTransactionTracker, ['quantity'])
                #ItemTransactions.objects.bulk_update_with_history(transBatch, ['used'])
                bulk_update_with_history(transBatch, ItemTransactions,['used'])
                return JsonResponse({'devMessage': 'success'})

                # print(batch)
            else:
                return JsonResponse({'devMessage': 'Quantity of Items are unavailable. Please refresh and check.'})
    except ObjectDoesNotExist:
        return JsonResponse({'devMessage': 'Does Not Exist: Some object does not exits'})


@role_required(allowed_roles=['manager', ])
@transaction.atomic
def delItemsToProj(request, pid, iid):

    try:
        if request.method == 'POST':

            item = ItemAvailability.objects.get(item=iid)
            project = ConstructionProject.objects.get(pk=pid)

            if project.frozen is True:
                return JsonResponse({'devMessage': 'Project Frozen, Items not reduced'})

            input_quantity = int(request.POST.get("quantity", ""))
            count = input_quantity
            tracker = ItemTransactionTracker.objects.filter(
                constProject=project, itemTrans__item=item.item).order_by('itemTrans__date')
            isAvailable = False
            batch = []
            batchUpdate = []
            transBatch = []

            for transaction in tracker:
                if (transaction.quantity <= count):
                    used = transaction.quantity
                    count -= used
                    transaction.quantity = 0  # to bulk delete later
                    transaction.itemTrans.used -= used
                    batchUpdate.append(transaction)
                    transBatch.append(transaction.itemTrans)
                    if (count == 0):
                        break
                elif (transaction.quantity > count):
                    transaction.quantity -= count
                    transaction.itemTrans.used -= count
                    count = 0
                    batchUpdate.append(transaction)
                    transBatch.append(transaction.itemTrans)
                    break

            if (count == 0):
                item.total_available += input_quantity
                item.total_used -= input_quantity

                #ItemTransactionTracker.objects.bulk_update_with_history(
                #    batchUpdate, ['quantity'])
                bulk_update_with_history(batchUpdate, ItemTransactionTracker, ['quantity'])
                #ItemTransactions.objects.bulk_update_with_history(transBatch, ['used'])
                bulk_update_with_history(transBatch, ItemTransactions, ['used'])
                item.save()
                ItemTransactionTracker.objects.filter(quantity=0).delete()
                return JsonResponse({'devMessage': 'success'})
            else:
                return JsonResponse({'devMessage': 'error'})
    except ObjectDoesNotExist:
        return JsonResponse({'devMessage': 'Does Not Exist: Some object does not exits'})


@role_required(allowed_roles=['manager', 'inventory'])
def statsPage(request):

    return render(request, 'construction_journal/stats.html')


@role_required(allowed_roles=['manager', 'inventory'])
def searchTags(request, tags):
    allTags = cleanTags(tags)
    return JsonResponse({'devMessage': 'success', 'message': allTags})


@role_required(allowed_roles=['manager', 'inventory'])
def searchProjsByTag(request, tags):
    allTags = cleanTags(tags)
    allProjects = ConstructionProject.objects.all()
    filteredProjs = None
    for string in allTags:
        filteredProjs = allProjects.filter(tags__icontains=string)
    jsonProjects = []
    for project in filteredProjs:

        total = ItemTransactionTracker.objects.filter(
            constProject=project).aggregate(totalCost=Sum(F('quantity') * F('itemTrans__price_per_piece'), output_field=FloatField()))
        if total.get('totalCost') is None:
            total['totalCost'] = 0
        # tp = transactions.itemTrans.price_per_piece * transactions.quantity
        aProject = {'pk': project.pk,
                    'job_order_num': project.job_order_num,
                    'project': project.project,
                    'client_info': project.client_info,
                    'address': project.address,
                    'tags': project.tags,
                    'total': str(total.get('totalCost'))

                    }
        jsonProjects.append(aProject)

    jsonProjects = json.dumps(jsonProjects, indent=4)
    return HttpResponse(jsonProjects, content_type="application/json")


@role_required(allowed_roles=['manager', 'inventory'])
def usedItemsByTag(request, tags):
    allTags = cleanTags(tags)
    allProjects = ConstructionProject.objects.all()
    filteredProjs = None
    items = None
    for string in allTags:
        filteredProjs = allProjects.filter(tags__icontains=string)
    jsonItems = []
    items = ItemTransactionTracker.objects.filter(
        constProject__in=filteredProjs)
    grouped_items = items.values('itemTrans__item__name',
                     'itemTrans__item__pk',
                     'itemTrans__item__item_code',
                     'itemTrans__item__unit',
                     'itemTrans__item__material').annotate(usedCount=Count('itemTrans__item__name')).order_by('-usedCount')

    for item in grouped_items:

        anItem = {'pk': item['itemTrans__item__pk'],
                  'name': item['itemTrans__item__name'],
                  'item_code': item['itemTrans__item__item_code'],
                  'unit': item['itemTrans__item__unit'],
                  'material': item['itemTrans__item__material'],
                  'used': item['usedCount'],
                  }
        jsonItems.append(anItem)

    jsonItems = json.dumps(jsonItems, indent=4)
    return HttpResponse(jsonItems, content_type="application/json")


def cleanTags(text):
    words = text
    splitCom = words.split(',')  # remove commas
    joinWords = ' '.join(splitCom)  # from array to string
    cleanTags = joinWords.split()  # split whitespace for clean tags
    return cleanTags


