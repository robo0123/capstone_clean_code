from django.forms import ModelForm
from .models import ConstructionProject
from django import forms

class ProjectForm(ModelForm):
    class Meta:
        model = ConstructionProject
        fields = (
            'job_order_num',
            'project',
            'address',
            'client_info',
            'tags',
            'remarks')
        widgets = {

            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
            'client_info': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Client Info', "rows": '4'}),
            'tags': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Tags', "rows": '4'}),
            'job_order_num': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Job Order'}),
            'project': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Project'}),
            'address': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Address'}),

        }

