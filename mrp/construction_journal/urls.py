from django.urls import path
from . import views

urlpatterns = [
    path('', views.constructionPage, name='construction_page'),
    path('load_projects_ajax/', views.loadProjectsAjax, name='loadProjectsAjax'),
    path('load_project_items_ajax/<int:id>', views.loadItemsToProj, name='loadItemsToProj'),
    path('viewmoreinfo/<int:pk>/', views.viewMoreProjectInfo,
         name='viewMoreProjectInfo'),
    path('delete/info/<int:id>', views.deleteProjectInfo, name='deleteProjectInfo'),
    path('update/info/<int:id>', views.updateProjectInfo, name='updateProjectInfo'),
    path('add/item/<int:pid>/<int:iid>/',
         views.addItemsToProj, name='addItemsToProj'),
    path('delete/item/<int:pid>/<int:iid>/',
         views.delItemsToProj, name='delItemsToProj'),
    path('stats', views.statsPage, name='stats_page'),
    path('search/<str:tags>/', views.searchTags, name='searchTags'),
    path('search/projs/<str:tags>/', views.searchProjsByTag, name='searchProjsByTag'),
    path('search/items/<str:tags>/', views.usedItemsByTag, name='usedItemsByTag'),
    path('freeze/project/<int:id>/', views.freezeProject, name='freezeProject'),

]
