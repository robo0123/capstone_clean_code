# Generated by Django 3.1.3 on 2020-11-21 06:21

from decimal import Decimal
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import simple_history.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('supplierDB', '0008_auto_20201115_1245'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ItemInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True)),
                ('item_code', models.CharField(blank=True, default='', max_length=80, null=True)),
                ('unit', models.CharField(max_length=30)),
                ('material', models.CharField(blank=True, max_length=70, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('srp', models.DecimalField(decimal_places=2, max_digits=17, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('time_created', models.DateTimeField(blank=True, default=django.utils.timezone.now, editable=False)),
                ('remarks', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ItemAvailability',
            fields=[
                ('item', models.OneToOneField(on_delete=django.db.models.deletion.RESTRICT, primary_key=True, serialize=False, to='inventory.iteminfo')),
                ('total_available', models.PositiveIntegerField(default=0)),
                ('total_used', models.PositiveIntegerField(default=0, validators=[django.core.validators.MaxValueValidator(models.PositiveIntegerField(default=0))])),
            ],
        ),
        migrations.CreateModel(
            name='KnownSuppliers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price_per_piece', models.DecimalField(decimal_places=2, max_digits=17, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('moq', models.PositiveIntegerField(default=0, validators=[django.core.validators.MinValueValidator(1)])),
                ('lead_time_days', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('remarks', models.TextField(blank=True, null=True)),
                ('itemInfo', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='inventory.iteminfo')),
                ('supplier', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='supplierDB.supplier')),
            ],
        ),
        migrations.CreateModel(
            name='KnownDiscounts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('discountRate', models.DecimalField(decimal_places=2, max_digits=3, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('moq', models.PositiveIntegerField()),
                ('knownSupplier', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='inventory.knownsuppliers')),
            ],
        ),
        migrations.CreateModel(
            name='ItemTransactions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveIntegerField(default=0, validators=[django.core.validators.MinValueValidator(1)])),
                ('price_per_piece', models.DecimalField(decimal_places=2, max_digits=17, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('used', models.PositiveIntegerField(default=0, validators=[django.core.validators.MaxValueValidator(models.PositiveIntegerField(default=0, validators=[django.core.validators.MinValueValidator(1)]))])),
                ('remarks', models.TextField(blank=True, null=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to=settings.AUTH_USER_MODEL)),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='inventory.iteminfo')),
                ('supplied_by', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='supplierDB.supplier')),
            ],
        ),
        migrations.CreateModel(
            name='HistoricalKnownSuppliers',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('price_per_piece', models.DecimalField(decimal_places=2, max_digits=17, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('moq', models.PositiveIntegerField(default=0, validators=[django.core.validators.MinValueValidator(1)])),
                ('lead_time_days', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('remarks', models.TextField(blank=True, null=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('itemInfo', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='inventory.iteminfo')),
                ('supplier', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='supplierDB.supplier')),
            ],
            options={
                'verbose_name': 'historical known suppliers',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalKnownDiscounts',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('discountRate', models.DecimalField(decimal_places=2, max_digits=3, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('moq', models.PositiveIntegerField()),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('knownSupplier', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='inventory.knownsuppliers')),
            ],
            options={
                'verbose_name': 'historical known discounts',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalItemTransactions',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('quantity', models.PositiveIntegerField(default=0, validators=[django.core.validators.MinValueValidator(1)])),
                ('price_per_piece', models.DecimalField(decimal_places=2, max_digits=17, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('used', models.PositiveIntegerField(default=0, validators=[django.core.validators.MaxValueValidator(models.PositiveIntegerField(default=0, validators=[django.core.validators.MinValueValidator(1)]))])),
                ('remarks', models.TextField(blank=True, null=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('created_by', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('item', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='inventory.iteminfo')),
                ('supplied_by', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='supplierDB.supplier')),
            ],
            options={
                'verbose_name': 'historical item transactions',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalItemInfo',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=200)),
                ('item_code', models.CharField(blank=True, default='', max_length=80, null=True)),
                ('unit', models.CharField(max_length=30)),
                ('material', models.CharField(blank=True, max_length=70, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('srp', models.DecimalField(decimal_places=2, max_digits=17, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('time_created', models.DateTimeField(blank=True, default=django.utils.timezone.now, editable=False)),
                ('remarks', models.TextField(blank=True, null=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical item info',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalItemAvailability',
            fields=[
                ('total_available', models.PositiveIntegerField(default=0)),
                ('total_used', models.PositiveIntegerField(default=0, validators=[django.core.validators.MaxValueValidator(models.PositiveIntegerField(default=0))])),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('item', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='inventory.iteminfo')),
            ],
            options={
                'verbose_name': 'historical item availability',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.AddConstraint(
            model_name='knownsuppliers',
            constraint=models.UniqueConstraint(fields=('supplier', 'itemInfo'), name='no same supplier per itemInfo'),
        ),
    ]
