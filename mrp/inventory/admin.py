from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from .models import ItemInfo, ItemTransactions, KnownSuppliers, KnownDiscounts

# Register your models here.

# read only and history


class ROAHAdmin(SimpleHistoryAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        pass

    def delete_model(self, request, obj):
        pass

    def save_related(self, request, form, formsets, change):
        pass

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save': False,
            'show_save_and_continue': False,
            'show_delete': False
        })
        return super().render_change_form(request, context, add, change, form_url, obj)


class ItemInfoAdmin(ROAHAdmin):
    readonly_fields = ('name', 'item_code', 'unit',
                       'material', 'description', 'srp', 'remarks')
    search_fields = ('name', 'item_code')

admin.site.register(ItemInfo, ItemInfoAdmin)


class ItemTransactionsAdmin(ROAHAdmin):
    readonly_fields = ('item', 'quantity', 'supplied_by', 'price_per_piece',
                       'date', 'created_by', 'used', 'remarks')
    search_fields = ('date', 'item__name')

admin.site.register(ItemTransactions, ItemTransactionsAdmin)


class KnownSuppliersAdmin(ROAHAdmin):
    readonly_fields = ('supplier', 'itemInfo', 'price_per_piece', 'moq',
                       'lead_time_days', 'remarks')
    search_fields = ('supplier__name', 'itemInfo__name')

admin.site.register(KnownSuppliers, KnownSuppliersAdmin)

class KnownDiscountsAdmin(ROAHAdmin):
    readonly_fields = ('knownSupplier', 'discountRate', 'moq')
    search_fields = ('knownSupplier__supplier__name', 'discountRate')

admin.site.register(KnownDiscounts, KnownDiscountsAdmin)

