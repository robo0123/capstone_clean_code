from django.shortcuts import render

# added below
from django.shortcuts import redirect, get_object_or_404
from .models import ItemInfo, ItemAvailability, ItemTransactions, KnownSuppliers, KnownDiscounts
from django.http import HttpResponse, JsonResponse
import json
from .forms import ItemForm, ItemTransactionForm, UpdateKnownSupplierToItemForm, LinkKnownSupplierToItemForm, AddDiscountRate
from django.contrib import messages
from django.contrib.auth.models import User
from django.db.models import RestrictedError
from django.db.utils import IntegrityError
from django.contrib.auth.hashers import check_password
from django.views.decorators.http import require_POST
from my_tools import query_debugger
from django.core import serializers
from django.db import transaction
from accounts.decorators import role_required
# Create your views here.


@role_required(allowed_roles=['manager', 'inventory'])
@transaction.atomic
def inventoryPage(request):
    form = ItemForm()
    context = {'form': form}

    if request.method == 'POST':
        form = ItemForm(request.POST)

        if form.is_valid():
            item = form.save(commit=False)
            item.author = User.objects.get(username=request.user)
            item.save()

            nameF = form.cleaned_data['name']
            itemObj = ItemInfo.objects.get(name=nameF)
            itemAva = ItemAvailability.objects.filter(item=itemObj).exists()
            if itemObj is not None and itemAva is not True:
                ItemAvailability.objects.create(item=itemObj)
                message = 'success'
                return JsonResponse({"form": form.as_p(), "devMessage": message})

        return JsonResponse({"form": form.as_p(), "devMessage": 'Error'})

    return render(request, 'inventory/inventory.html', context)


@role_required(allowed_roles=['manager', 'inventory'])
def loadItemsAjax(request):

    allItems = ItemAvailability.objects.select_related('item').all()  # join
    jsonItems = []
    for eachItem in allItems:

        anItem = {'pk': eachItem.item.pk,
                  'name': eachItem.item.name,
                  'item_code': eachItem.item.item_code,
                  'unit': eachItem.item.unit,
                  'material': eachItem.item.material,
                  'srp': str(eachItem.item.srp),
                  'description': eachItem.item.description,
                  'remarks': eachItem.item.remarks,
                  'available': eachItem.total_available,
                  'used': eachItem.total_used
                  }
        jsonItems.append(anItem)

    jsonItems = json.dumps(jsonItems, indent=4)

    return HttpResponse(jsonItems, content_type="application/json")


@role_required(allowed_roles=['manager', 'inventory'])
def addNewItemAjax(request):
    form = ItemForm()

    if request.method == 'POST':
        form = ItemForm(request.POST)

        if form.is_valid():
            item = form.save(commit=False)
            item.author = User.objects.get(username=request.user)
            item.save()

            nameF = form.cleaned_data['name']
            itemObj = ItemInfo.objects.get(name=nameF)
            if itemObj is not None:
                ItemAvailability.objects.create(item=itemObj)

    return HttpResponse(form.as_p())


@role_required(allowed_roles=['manager', 'inventory'])
def viewMoreItemInfo(request, pk):
    itemInfo = get_object_or_404(ItemInfo, pk=pk)
    itemAvail = get_object_or_404(ItemAvailability, pk=pk)
    updateForm = ItemForm(instance=itemInfo)
    transactionForm = ItemTransactionForm()
    linkSupplierForm = LinkKnownSupplierToItemForm(instance=None, itemPK=pk)
    discountForm = AddDiscountRate()

    context = {'updateForm': updateForm,
               'transForm': transactionForm,
               'itemInfo': itemInfo,
               'itemAvail': itemAvail,
               'linkForm': linkSupplierForm,
               'discountForm': discountForm
               }
    return render(request, 'inventory/moreinfo.html', context)


@role_required(allowed_roles=['manager', ])
@require_POST
def deleteItemInfo(request, id):

    if request.method == 'POST':
        password = request.POST.get("password", "")
        correct_password = check_password(password, request.user.password)

        if correct_password:
            item = None
            message = None
            delete_success = False
            try:
                with transaction.atomic():  # when error occurs, stuff is rolled back
                    item = ItemInfo.objects.get(pk=id)
                    availItem = ItemAvailability.objects.get(item=item)
                    if availItem.total_available == 0 and availItem.total_used == 0:
                        availItem.delete()
                        item.delete()
                        delete_success = True
                        message = 'success'
                    else:
                        message = 'Error: Item already being used.'
            except RestrictedError:
                delete_success = False
                message = 'Restricted Error: A part of the system is using this ItemInfo. You must delete those first.'
            except ItemInfo.DoesNotExist:
                delete_success = False
                message = 'DoesNotExist Error: The Item Info does not exist'

            return JsonResponse({'delete_success': delete_success, 'devMessage': message})
        else:
            return JsonResponse({'delete_success': 'False', 'devMessage': 'Wrong Password'})


@role_required(allowed_roles=['manager', 'inventory'])
@require_POST
@transaction.atomic
def updateItemInfo(request, id):
    if request.method == 'POST':
        try:
            instance = ItemInfo.objects.get(pk=id)
        except ItemInfo.DoesNotExist:
            form = ItemForm()
            message = 'DoesNotExist Error: The Item Info does not exist'
            return JsonResponse({'devMessage': message, 'form': form.as_p()})

        form = ItemForm(request.POST, instance=instance)
        if form.is_valid():
            form.save()
            return JsonResponse({'devMessage': 'success', 'form': form.as_p()})
    return JsonResponse({'devMessage': 'Error', 'form': form.as_p()})


@role_required(allowed_roles=['manager', 'inventory'])
@require_POST
@transaction.atomic
def addTransaction(request, id):

    if request.method == 'POST':

        form = ItemTransactionForm(request.POST, id)

        if form.is_valid():
            transaction = form.save(commit=False)
            try:
                item = ItemInfo.objects.get(pk=id)
            except ItemInfo.DoesNotExist:
                return JsonResponse({"form": form.as_p(), "devMessage": 'Does Not Exist: Item Info'})
            transaction.item = item
            transaction.created_by = request.user
            transaction.save()
            # have to unpack when using get_or_create
            # transacts with ItemAvailability
            availItem, created = ItemAvailability.objects.get_or_create(
                item=item)
            availItem.total_available += transaction.quantity
            availItem.save()

            sup = form['supplied_by'].value()
            supClean = form.cleaned_data['supplied_by']
            supName = supClean.name
            extraMessage = None
            if (KnownSuppliers.objects.filter(supplier=sup, itemInfo=item).exists()) is False:

                KnownSuppliers.objects.create(supplier=supClean,
                                              itemInfo=item,
                                              price_per_piece=form.cleaned_data['price_per_piece'],
                                              moq=form.cleaned_data['quantity'],
                                              lead_time_days=0,
                                              remarks="Automatically Added By System.")

                extraMessage = 'Supplier: "' + supName + '" was not linked with Known Suppliers Table.\n' + \
                    'System automatically linked Supplier: "' + supName + '" to Known Suppliers table. \n' + \
                    'Please fix details in Known Supplier table if necessary.'

            return JsonResponse({"form": form.as_p(), "devMessage": 'success', 'extraMessage': extraMessage})

        return JsonResponse({"form": form.as_p(), "devMessage": 'Error'})


@role_required(allowed_roles=['manager', 'inventory'])
def loadTransactionsAjax(request, id):
    transactions = ItemTransactions.objects.filter(item=id)
    jsonTransactions = []
    for transaction in transactions:
        total = (transaction.quantity * transaction.price_per_piece)
        aTransaction = {'pk': transaction.pk,
                        'created_by': str(transaction.created_by),
                        'supplied_by': str(transaction.supplied_by),
                        'quantity': transaction.quantity,
                        'used': transaction.used,
                        'price_per_piece': str(transaction.price_per_piece),
                        'total': str(total),
                        'remarks': transaction.remarks,
                        'date': str(transaction.date)
                        }
        jsonTransactions.append(aTransaction)

    jsonTransactions = json.dumps(jsonTransactions, indent=4)
    return HttpResponse(jsonTransactions, content_type="application/json")


@role_required(allowed_roles=['manager', ])
@require_POST
def deleteTransaction(request, id):

    if request.method == 'POST':
        password = request.POST.get("password", "")
        correct_password = check_password(password, request.user.password)

        if correct_password:
            trans = None
            message = None
            delete_success = False
            try:
                with transaction.atomic():
                    trans = ItemTransactions.objects.get(pk=id)
                    trans_item_id = trans.item_id
                    trans_qty = trans.quantity
                    trans.delete()
                    delete_success = True
                    item = ItemInfo.objects.get(pk=trans_item_id)
                    availItem = ItemAvailability.objects.get(item=item)
                    availItem.total_available -= trans_qty
                    availItem.save()
                    message = 'success'
            except RestrictedError:
                delete_success = False
                message = 'Restricted Error: A part of the system is using this Transaction Info. You must delete those first.'
            except ItemTransactions.DoesNotExist:
                delete_success = False
                message = 'DoesNotExist Error: The Item Transaction does not exist'
            except ItemInfo.DoesNotExist:
                delete_success = False
                message = 'DoesNotExist Error: The Item Info does not exist'
            except ItemAvailability.DoesNotExist:
                delete_success = False
                message = 'DoesNotExist Error: The Item Availability does not exist'

            return JsonResponse({'delete_success': delete_success, 'devMessage': message})
        else:
            return JsonResponse({'delete_success': 'False', 'devMessage': 'Wrong Password'})


@role_required(allowed_roles=['manager', ])
def linkSupplierToItem(request, id):
    if request.method == 'GET':
        linkSupplierForm = LinkKnownSupplierToItemForm(
            instance=None, itemPK=id)
        return JsonResponse({"form": linkSupplierForm.as_p()})

    if request.method == 'POST':

        form = LinkKnownSupplierToItemForm(id, request.POST)

        try:
            with transaction.atomic():
                if form.is_valid():
                    linkForm = form.save(commit=False)
                    item = ItemInfo.objects.get(pk=id)
                    linkForm.itemInfo = item
                    linkForm.save()
                    linkSupplierForm = LinkKnownSupplierToItemForm(
                        instance=None, itemPK=id)
                    return JsonResponse({"form": linkSupplierForm.as_p(), "devMessage": 'success'})
        except IntegrityError:
            ieError = "Some Integrity Error occured"
            return JsonResponse({"devMessage": ieError, 'form': form.as_p()})
        except ItemInfo.DoesNotExist:
            error = "Does Not Exist: Item Info"
            return JsonResponse({"devMessage": error, 'form': form.as_p()})

        return JsonResponse({"form": form.as_p(), "devMessage": 'Error'})


@role_required(allowed_roles=['manager', ])
def loadLinkedSuppliersAjax(request, id):
    allSuppliers = KnownSuppliers.objects.filter(itemInfo=id)  # join
    jsonSuppliers = []
    for eachSupplier in allSuppliers:

        discounts_json = []
        discounts = KnownDiscounts.objects.filter(
            knownSupplier=eachSupplier.pk)

        for eachDiscount in discounts:
            each = {
                'pk': eachDiscount.pk,
                'moq': eachDiscount.moq,
                'discount_rate': str(eachDiscount.discountRate)
            }
            discounts_json.append(each)

        eachSupplier = {'pk': eachSupplier.pk,
                        'supPK': eachSupplier.supplier.pk,
                        'supplier': eachSupplier.supplier.name,
                        'moq': eachSupplier.moq,
                        'price_per_piece': str(eachSupplier.price_per_piece),
                        'lead_time': eachSupplier.lead_time_days,
                        'rating': eachSupplier.supplier.rating,
                        'remarks': eachSupplier.remarks,
                        'discounts': discounts_json
                        }
        jsonSuppliers.append(eachSupplier)

    jsonSuppliers = json.dumps(jsonSuppliers, indent=4)

    return HttpResponse(jsonSuppliers, content_type="application/json")


@role_required(allowed_roles=['manager', ])
def deleteKnownSuppplierLink(request, id):

    if request.method == 'POST':
        password = request.POST.get("password", "")
        correct_password = check_password(password, request.user.password)

        if correct_password:
            knownSup = None
            message = None
            delete_success = False
            try:
                with transaction.atomic():
                    knownSup = KnownSuppliers.objects.get(pk=id)
                    knownSup.delete()
                    message = 'success'
            except RestrictedError:
                delete_success = False
                message = 'Restricted Error: A part of the system is using this Known Supplier. You must delete those first.'
            except KnownSuppliers.DoesNotExist:
                delete_success = False
                message = 'DoesNotExist Error: KnownSupplier entry does not exist. Please Refresh Table.'

            return JsonResponse({'delete_success': delete_success, 'devMessage': message})
        else:
            return JsonResponse({'delete_success': 'False', 'devMessage': 'Wrong Password'})


@role_required(allowed_roles=['manager', ])
@transaction.atomic
def updateKnownSupplierLink(request, id):

    if request.method == 'POST':
        try:
            instance = KnownSuppliers.objects.get(pk=id)
        except ItemInfo.DoesNotExist:
            message = "Does Not Exist: Link may have been deleted already."
            form = UpdateKnownSupplierToItemForm()
            return JsonResponse({"devMessage": message, 'form': form.as_p()})

        form = UpdateKnownSupplierToItemForm(request.POST, instance=instance)
        ks = None
        try:
            if form.is_valid():
                kd = KnownDiscounts.objects.filter(knownSupplier=instance)
                inpMOQ = form.cleaned_data['moq']
                for objs in kd:
                    if inpMOQ > objs.moq:
                        err = 'Changes not saved: There is a known discount with a lower MOQ. You have to delete those discounts first.'
                        return JsonResponse({'devMessage': err, 'form': form.as_p()})
                        # this checks MOQ of discounts first. It has to make sense

                form.save()
                return JsonResponse({'devMessage': 'success', 'form': form.as_p()})
        except IntegrityError:
            ieError = "IntegrityError: Name can't be the same with other names."
            return JsonResponse({"devMessage": ieError, 'form': form.as_p()})

        return JsonResponse({'devMessage': 'error', 'form': form.as_p()})

    try:
        ksInfo = KnownSuppliers.objects.get(pk=id)
    except KnownSuppliers.DoesNotExist:
        updateForm = UpdateKnownSupplierToItemForm()
        return JsonResponse({'error': 'Entry Does Not Exist - Please refresh table', 'form': updateForm.as_p()})

    updateForm = UpdateKnownSupplierToItemForm(instance=ksInfo)
    return JsonResponse({'form': updateForm.as_p()})


@role_required(allowed_roles=['manager',])
@require_POST
@transaction.atomic
def addDiscount(request, id):

    if request.method == 'POST':

        form = AddDiscountRate(request.POST)

        if form.is_valid():
            discount = form.save(commit=False)
            try:
                knownSupplier = KnownSuppliers.objects.get(pk=id)
            except KnownSuppliers.DoesNotExist:
                return JsonResponse({"form": form.as_p(), "devMessage": 'DoesNotExist: Known Supplier does not exist.  Discount not Added. Refresh Table.'})

            moq = form.cleaned_data['moq']

            if knownSupplier.moq >= moq:
                return JsonResponse({"form": form.as_p(), "devMessage": 'Error. Input MOQ is less than or equal to known MOQ. Discount not added'})
            else:
                discount.knownSupplier = knownSupplier
                discount.save()
                return JsonResponse({"form": form.as_p(), "devMessage": 'success'})

        return JsonResponse({"form": form.as_p(), "devMessage": 'Error'})


@role_required(allowed_roles=['manager', ])
@require_POST
@transaction.atomic
def deleteDiscount(request, id):

    if request.method == 'POST':
        password = request.POST.get("password", "")
        correct_password = check_password(password, request.user.password)

        if correct_password:
            discount = None
            message = None
            delete_success = False
            try:
                discount = KnownDiscounts.objects.get(pk=id)
                discount.delete()
                message = 'success'
            except RestrictedError:
                delete_success = False
                message = 'Restricted Error: A part of the system is using this Known Discount. You must delete those first.'
            except KnownDiscounts.DoesNotExist:
                delete_success = False
                message = 'Does Not Exist Error: Discount does not exist, refresh table.'

            return JsonResponse({'delete_success': delete_success, 'devMessage': message})
        else:
            return JsonResponse({'delete_success': 'False', 'devMessage': 'Wrong Password'})


@role_required(allowed_roles=['manager', 'inventory'])
def refreshAvailAjax(request, id):
    itemAvail = get_object_or_404(ItemAvailability, pk=id)
    return JsonResponse({'available': itemAvail.total_available, 'used': itemAvail.total_used})
