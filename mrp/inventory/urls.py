from django.urls import path
from . import views

urlpatterns = [
    path('', views.inventoryPage, name='inventory_page'),
    path('load_items_ajax/', views.loadItemsAjax, name='loadItemsAjax'),
    path('add_new_item_ajax/', views.addNewItemAjax, name='addNewItemAjax'),
    path('viewmoreinfo/<int:pk>/', views.viewMoreItemInfo, name='viewMoreItemInfo'),
    path('add/transaction/<int:id>', views.addTransaction, name='addTransaction'),
    path('delete/info/<int:id>', views.deleteItemInfo, name='deleteItemInfo'),
    path('update/info/<int:id>', views.updateItemInfo, name='updateItemInfo'),
    path('load_transactions_ajax/<int:id>', views.loadTransactionsAjax, name='loadTransactionsAjax'),
    path('delete/transaction/<int:id>', views.deleteTransaction, name='deleteTransaction'),
    path('link/supplier/<int:id>', views.linkSupplierToItem, name='linkSupplierToItem'),
    path('load_link_suppliers_ajax/<int:id>', views.loadLinkedSuppliersAjax, name='loadLinkSuppliers'),
    path('delete/known_supplier/<int:id>', views.deleteKnownSuppplierLink, name='deleteKnownSuppplierLink'),
    path('update/known_supplier/<int:id>', views.updateKnownSupplierLink, name='updateKnownSuppplierLink'),
    path('add/discount/<int:id>', views.addDiscount, name='addDiscount'),
    path('delete/known_discount/<int:id>', views.deleteDiscount, name='deleteKnownDiscount'),
    path('refresh/availability/<int:id>', views.refreshAvailAjax, name='refreshAvailAjax'),
    
    
   
]