from django.forms import ModelForm
from .models import ItemInfo, ItemTransactions, KnownSuppliers, KnownDiscounts
from supplierDB.models import Supplier
from django import forms
from django.forms.fields import DateInput
from django.db.models import Q
from datetime import date
import datetime

today = date.today()
minDate = datetime.datetime.strptime('19500101', '%Y%m%d').date()


def exceed_max_length(text, num):
    return len(text) > num


class ItemForm(ModelForm):
    class Meta:
        model = ItemInfo
        fields = ('name',
                  'item_code',
                  'unit',
                  'material',
                  'srp',
                  'description',
                  'remarks')
        labels = {'srp': 'Suggested Retail Price'}
        widgets = {
            'name': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Item Name'}),
            'item_code': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Item Code'}),
            'unit': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Unit'}),
            'material': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Material'}),
            'srp': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'SRP', 'step': "0.01"}),
            'description': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Description', "rows": '4'}),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),



        }
    # this function will be used for the validation
    # might not need this because django implements a max length already

    def clean_name(self):
        data = self.cleaned_data.get('name')

        if exceed_max_length(data, ItemInfo._meta.get_field('name').max_length):
            raise forms.ValidationError(
                'Name is too long. Max is 200 characters.')

        return data


class ItemTransactionForm(ModelForm):
    class Meta:
        model = ItemTransactions
        fields = (
            'quantity',
            'supplied_by',
            'price_per_piece',
            'date',
            'remarks')

        widgets = {
            'date': forms.DateInput(format=('%Y/%m/%d'),
                                    attrs={'class': 'datepicker input', 'type': 'date',
                                           'min': str(minDate), 'max': str(today)}
                                    ),

            'quantity': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'Quantity'}),
            'price_per_piece': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'SRP', 'step': "0.01"}),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
        }

    def clean_date(self):
        date = self.cleaned_data['date']
        if date > today or date < minDate:
            raise forms.ValidationError(
                'Inputted date is outside of allowed range.')
        return date

    def __init__(self, *args, **kwargs):
        super(ItemTransactionForm, self).__init__(*args, **kwargs)
        self.fields['supplied_by'].queryset = Supplier.objects.order_by('name')


class LinkKnownSupplierToItemForm(ModelForm):
    class Meta:
        model = KnownSuppliers
        fields = (
            'supplier',
            'price_per_piece',
            'moq',
            'lead_time_days',
            'remarks')
        widgets = {
            'price_per_piece': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'SRP', 'step': "0.01"}),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
            'moq': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'Minimum Order Quantity'}),
            'lead_time_days': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'Lead Time in Days'}),
        }
        labels = {
            "moq": "Minimum Order Quantity",
            'lead_time_days': 'Lead Time (Days)'
        }

    def __init__(self, itemPK, *args, **kwargs):
        super(LinkKnownSupplierToItemForm, self).__init__(*args, **kwargs)
        if self.instance:
            knownSup = KnownSuppliers.objects.filter(
                itemInfo=itemPK).values_list('supplier_id', flat=True)
            supPKList = list(knownSup)
            listSup = Supplier.objects.exclude(pk__in=supPKList)
            self.fields['supplier'].queryset = listSup.order_by('name')


class UpdateKnownSupplierToItemForm(ModelForm):
    class Meta:
        model = KnownSuppliers
        fields = (
            'price_per_piece',
            'moq',
            'lead_time_days',
            'remarks')
        labels = {
            "moq": "Minimum Order Quantity",
            'lead_time_days': 'Lead Time (Days)'
        }
        widgets = {
            'price_per_piece': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'SRP', 'step': "0.01"}),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
            'moq': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'Minimum Order Quantity'}),
            'lead_time_days': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'Lead Time in Days'}),
        }


class AddDiscountRate(ModelForm):
    class Meta:
        model = KnownDiscounts
        fields = (
            'moq',
            'discountRate',
        )

        widgets = {
            'discountRate': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'SRP'}),
            'moq': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'Minimum Order Quantity'}),
        }

        labels = {
            "moq": "Minimum Order Quantity",
            'discountRate': 'Discount Rate %'
        }