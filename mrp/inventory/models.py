from django.db import models

# added
from supplierDB.models import Supplier
from django.contrib.auth.models import User
from django.utils.timezone import now
from simple_history.models import HistoricalRecords
from django.core.validators import MinValueValidator, MaxValueValidator
from decimal import Decimal
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import datetime
# added

# Create your models here.


class ItemInfo(models.Model):
    name = models.CharField(max_length=200, blank=False,
                            null=False, unique=True)
    # brand = models.CharField(max_length=140, blank=True, null=True)
    item_code = models.CharField(
        max_length=80, blank=True, null=True, default='')
    # quantity = models.IntegerField(blank=False, null=False)
    unit = models.CharField(  # for example: box or pieces
        max_length=30, blank=False, null=False)
    material = models.CharField(max_length=70, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    # up to trillions place with 2 decimal places
    srp = models.DecimalField(
        decimal_places=2, max_digits=17, blank=False, null=False, validators=[MinValueValidator(Decimal('0.01'))])
    #author = models.ForeignKey(User, on_delete=models.RESTRICT)
    time_created = models.DateTimeField(
        default=now, editable=False, blank=True, null=False)
    remarks = models.TextField(blank=True, null=True)
    history = HistoricalRecords()

    def __str__(self):
        if self.item_code is None:
            return self.name
        else:
            nameAndItem_code = (self.name + ' ' + self.item_code)
            return nameAndItem_code


class ItemTransactions(models.Model):
    item = models.ForeignKey(
        ItemInfo, on_delete=models.RESTRICT, blank=False, null=False)
    quantity = models.PositiveIntegerField(
        blank=False, null=False, default=0, validators=[MinValueValidator(1)])
    supplied_by = models.ForeignKey(
        Supplier, on_delete=models.RESTRICT, blank=False, null=False)
    price_per_piece = models.DecimalField(
        decimal_places=2, max_digits=17, blank=False, null=False, validators=[MinValueValidator(Decimal('0.01'))])
    date = models.DateField(blank=False, null=False,
                            default=datetime.date.today)
    created_by = models.ForeignKey(User, on_delete=models.RESTRICT)
    used = models.PositiveIntegerField(
        blank=False, null=False, default=0)
    remarks = models.TextField(blank=True, null=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.item.name + ' Date: ' + str(self.date)

    def clean(self, *args, **kwargs):
        if self.used > self.quantity:
            raise ValidationError(
                _('Ensure this value is greater than or equal to %(value)s'),
                params={'value': self.quantity},
            )

        super().clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)



class ItemAvailability(models.Model):
    item = models.OneToOneField(
        ItemInfo, on_delete=models.RESTRICT, blank=False, null=False, primary_key=True)
    total_available = models.PositiveIntegerField(
        blank=False, null=False, default=0)
    total_used = models.PositiveIntegerField(
        blank=False, null=False, default=0)
    history = HistoricalRecords()

    def clean(self, *args, **kwargs):
        if self.total_used > self.total_available:
            raise ValidationError(
                _('Ensure this value is greater than or equal to %(value)s'),
                params={'value': self.total_available},
            )

        super().clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)


class KnownSuppliers(models.Model):
    supplier = models.ForeignKey(
        Supplier, on_delete=models.RESTRICT, blank=False, null=False)
    itemInfo = models.ForeignKey(
        ItemInfo, on_delete=models.RESTRICT, blank=False, null=False)
    price_per_piece = models.DecimalField(
        decimal_places=2, max_digits=17, blank=False, null=False, validators=[MinValueValidator(Decimal('0.01'))])
    # minimum order quantity
    moq = models.PositiveIntegerField(
        blank=False, null=False, default=0, validators=[MinValueValidator(1)])
    lead_time_days = models.PositiveIntegerField(
        blank=True, null=True, default=0)
    remarks = models.TextField(blank=True, null=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.supplier.name + ' - ' + self.itemInfo.name

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['supplier', 'itemInfo'],
                name='no same supplier per itemInfo'
            )
        ]


class KnownDiscounts(models.Model):
    knownSupplier = models.ForeignKey(
        KnownSuppliers, on_delete=models.RESTRICT, blank=False, null=False)
    discountRate = models.PositiveIntegerField(blank=False, null=False, validators=[
                                               MinValueValidator(1), MaxValueValidator(100)])
    # minimum order quantity
    moq = models.PositiveIntegerField(blank=False, null=False)

    history = HistoricalRecords()

    def __str__(self):
        return self.knownSupplier.supplier.name + ' - ' + str(self.discountRate) + '%'