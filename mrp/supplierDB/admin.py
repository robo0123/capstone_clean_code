from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from .models import Supplier, KeyPersonnel, ContactInfo

# Register your models here.


class ROAHAdmin(SimpleHistoryAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        pass

    def delete_model(self, request, obj):
        pass

    def save_related(self, request, form, formsets, change):
        pass

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save': False,
            'show_save_and_continue': False,
            'show_delete': False
        })
        return super().render_change_form(request, context, add, change, form_url, obj)


class SupplierAdmin(ROAHAdmin):
    readonly_fields = ('name', 'tin', 'address',
                       'rating', 'sellerType', 'remarks')
    search_fields = ('name',)


admin.site.register(Supplier, SupplierAdmin)


class KeyPersonnelAdmin(ROAHAdmin):
    readonly_fields = ('supplier', 'name', 'description', 'color_code',
                       'remarks')
    search_fields = ('name',)


admin.site.register(KeyPersonnel, KeyPersonnelAdmin)


class ContactInfoAdmin(ROAHAdmin):
    readonly_fields = ('supplier', 'keyPersonnel', 'phone_number', 'email_address', 'description',
                       'remarks')
    search_fields = ('supplier__name',)


admin.site.register(ContactInfo, ContactInfoAdmin)
