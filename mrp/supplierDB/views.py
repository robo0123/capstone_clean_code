from django.shortcuts import render

# added below
from django.shortcuts import redirect, get_object_or_404
from .models import Supplier, KeyPersonnel, ContactInfo
from django.http import HttpResponse, JsonResponse
import json
from .forms import SupplierForm, KeyPersonnelForm, ContactInfoForm, ItemTransactionSupForm, LinkKnownItemToSupplierForm, UpdateKPForm, UpdateContactForm
from django.contrib import messages
from django.contrib.auth.models import User
from django.db.models import RestrictedError
from django.db.utils import IntegrityError
from django.contrib.auth.hashers import check_password
from django.views.decorators.http import require_POST
from inventory.models import KnownSuppliers, KnownDiscounts, ItemTransactions, ItemAvailability
from inventory.forms import AddDiscountRate
from inventory import views as inventory_view
import phonenumbers
from django.db import transaction
from accounts.decorators import role_required
# Create your views here.

# Create your views here.

@role_required(allowed_roles=['manager',])
def supplierPage(request):
    form = SupplierForm()
    context = {'form': form}

    if request.method == 'POST':
        form = SupplierForm(request.POST)
        # print(form)
        if form.is_valid():
            form.save()
            message = 'success'
            return JsonResponse({"form": form.as_p(), "devMessage": message})

        return JsonResponse({"form": form.as_p(), "devMessage": 'error'})

    return render(request, 'supplierDB/supplier.html', context)


@role_required(allowed_roles=['manager',])
def loadSuppliersAjax(request):
    
    allSuppliers = Supplier.objects.all()
    jsonSuppliers = []
    for supplier in allSuppliers:
        aSupplier = {'pk': supplier.pk,
                     'name': supplier.name,
                     'tin': supplier.tin,
                     'address': supplier.address,
                     'rating': supplier.rating,
                     'sellerType': supplier.sellerType,
                     'remarks': supplier.remarks,
                     # 'close_down': supplier.close_down,
                     }
        jsonSuppliers.append(aSupplier)

    jsonSuppliers = json.dumps(jsonSuppliers, indent=4)

    return HttpResponse(jsonSuppliers, content_type="application/json")


@role_required(allowed_roles=['manager',])
def viewMoreSupplierInfo(request, pk):
    supplierInfo = get_object_or_404(Supplier, pk=pk)
    updateForm = SupplierForm(instance=supplierInfo)
    keyPersonnelForm = KeyPersonnelForm()
    contactInfoForm = ContactInfoForm(pk)
    transForm = ItemTransactionSupForm()
    linkSupplierForm = LinkKnownItemToSupplierForm(
        instance=None, supplierPK=pk)
    discountForm = AddDiscountRate()

    context = {'updateForm': updateForm,
               'supplierInfo': supplierInfo,
               'keyPersonnel': keyPersonnelForm,
               'contactInfoForm': contactInfoForm,
               'transForm': transForm,
               'linkForm': linkSupplierForm,
               'discountForm': discountForm
               }
    return render(request, 'supplierDB/moreinfo.html', context)


@role_required(allowed_roles=['manager',])
@require_POST
def deleteSupplierInfo(request, id):

    if request.method == 'POST':
        password = request.POST.get("password", "")
        correct_password = check_password(password, request.user.password)

        if correct_password:
            supplier = None
            message = None
            delete_success = False
            try:
                supplier = Supplier.objects.get(pk=id)
                supplier.delete()
                delete_success = True
                message = 'success'
            except RestrictedError:
                delete_success = False
                message = 'Restricted Error: A part of the system is using this Supplier. You must delete those first.'
            except Supplier.DoesNotExist:
                delete_success = False
                message = 'Does Not Exist: Supplier does not exist anymore. May have already been deleted.'

            return JsonResponse({'devMessage': message})
        else:
            return JsonResponse({'devMessage': 'Wrong Password'})


@role_required(allowed_roles=['manager',])
@require_POST
def updateSupplierInfo(request, id):
    if request.method == 'POST':
        try:
            instance = Supplier.objects.get(pk=id)
        except Supplier.DoesNotExist:
            delete_success = False
            message = 'Does Not Exist: Supplier does not exist anymore. May have already been deleted.'
            form = SupplierForm()
            return JsonResponse({'devMessage': message, 'form': form.as_p()})

        form = SupplierForm(request.POST, instance=instance)
        supplier = None
        if form.is_valid():
            form.save()
            supplier = get_object_or_404(Supplier, pk=id)
            return JsonResponse({'devMessage': 'success', 'form': form.as_p()})
    return JsonResponse({'devMessage': 'Error', 'form': form.as_p()})


@role_required(allowed_roles=['manager',])
@require_POST
def addKeyPersonnel(request, id):

    if request.method == 'POST':
        form = KeyPersonnelForm(request.POST)
        try:
            if form.is_valid():
                kp = form.save(commit=False)
                kp.supplier = Supplier.objects.get(pk=id)
                kp.save()
                message = 'Successfully Created'
                return JsonResponse({"devMessage": 'success'})
        except IntegrityError:
            ieError = "IntegrityError: Name can't be the same with other names."
            return JsonResponse({"devMessage": ieError})
        except Supplier.DoesNotExist:
                message = "Does Not Exist: Can't link key personnel because Supplier does not exist anymore."
                return JsonResponse({'devMessage': message})

    return JsonResponse({"devMessage": 'error'})


@role_required(allowed_roles=['manager',])
def loadKeyPersonnelAjax(request, id):
    sup = Supplier.objects.get(pk=id)
    allKP = KeyPersonnel.objects.filter(supplier=sup)
    jsonKP = []
    for kp in allKP:
        akp = {'pk': kp.pk,
               'name': kp.name,
               'description': kp.description,
               'color_code': kp.color_code,
               'remarks': kp.remarks,

               }
        jsonKP.append(akp)

    jsonKP = json.dumps(jsonKP, indent=4)
    return HttpResponse(jsonKP, content_type="application/json")


@role_required(allowed_roles=['manager',])
def loadContactInfoAjax(request, id):
    
    sup = Supplier.objects.get(pk=id)
    allContact = ContactInfo.objects.filter(supplier=sup)
    jsonAC = []
    for info in allContact:
        if info.keyPersonnel is None:
            akp = {'pk': info.pk,
                   'kp': '',
                   'phone_number': phonenumbers.format_number(info.phone_number, phonenumbers.PhoneNumberFormat.INTERNATIONAL),
                   'email_address': info.email_address,
                   'description': info.description,
                   'remarks': info.remarks,
                   }
        else:
            akp = {'pk': info.pk,
                   'kp': info.keyPersonnel.name,
                   'phone_number': phonenumbers.format_number(info.phone_number, phonenumbers.PhoneNumberFormat.INTERNATIONAL),
                   'email_address': info.email_address,
                   'description': info.description,
                   'remarks': info.remarks,
                   }
        jsonAC.append(akp)

    jsonAC = json.dumps(jsonAC, indent=4)
    return HttpResponse(jsonAC, content_type="application/json")


@role_required(allowed_roles=['manager',])
def loadKPAndConAjax(request, id):
   
    sup = Supplier.objects.get(pk=id)
    allContact = ContactInfo.objects.filter(supplier=sup)
    jsonAC = []
    for info in allContact:
        num = None
        if info.phone_number is None:
            num = None
        else:
            num = phonenumbers.format_number(info.phone_number, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
        if info.keyPersonnel is None:
            akp = {'pk': info.pk,
                   'kp': '',
                   'phone_number': num,
                   'email_address': info.email_address,
                   'description': info.description,
                   'remarks': info.remarks,
                   'color_code': '',
                   'emp_description': '',
                   'emp_remarks': ''
                   }
        else:
            akp = {'pk': info.pk,
                   'kp': info.keyPersonnel.name,
                   'phone_number': num,
                   'email_address': info.email_address,
                   'description': info.description,
                   'remarks': info.remarks,
                   'color_code': info.keyPersonnel.color_code,
                   'emp_description': info.keyPersonnel.description,
                   'emp_remarks': info.keyPersonnel.remarks
                   }
        jsonAC.append(akp)

    jsonAC = json.dumps(jsonAC, indent=4)
    return HttpResponse(jsonAC, content_type="application/json")


@role_required(allowed_roles=['manager',])
def deleteKP(request, id):

    if request.method == 'POST':
        password = request.POST.get("password", "")
        correct_password = check_password(password, request.user.password)

        if correct_password:
            kp = None
            message = None
            delete_success = False
            try:
                kp = KeyPersonnel.objects.get(pk=id)
                kp.delete()
                delete_success = True
                message = 'success'
            except RestrictedError:
                delete_success = False
                message = 'Restricted Error: A part of the system is using this Key Personnel. You must delete those entries first.'
            except KeyPersonnel.DoesNotExist:
                delete_success = False
                message = 'Does Not Exist: Entry does not exist anymore, please refresh table.'

            return JsonResponse({'delete_success': delete_success, 'devMessage': message})
        else:
            return JsonResponse({'delete_success': 'False', 'devMessage': 'Wrong Password'})


@role_required(allowed_roles=['manager',])
def updateKPInfo(request, id):

    if request.method == 'POST':
        try:
            instance = KeyPersonnel.objects.get(pk=id)
        except KeyPersonnel.DoesNotExist:
            form = KeyPersonnelForm()
            message = 'Does Not Exist: Entry may not exist anymore, please refresh table.'
            return JsonResponse({'devMessage': message, 'form': form.as_p()})

        form = KeyPersonnelForm(request.POST, instance=instance)
        kp = None
        try:
            if form.is_valid():
                form.save()
                return JsonResponse({'devMessage': 'success', 'form': form.as_p()})
        except IntegrityError:
            ieError = "IntegrityError: Name can't be the same with other names."
            return JsonResponse({"devMessage": ieError, 'form': form.as_p()})

        return JsonResponse({'devMessage': 'error', 'form': form.as_p()})

    if request.method == 'GET':
        kpInfo = get_object_or_404(KeyPersonnel, pk=id)
        updateForm = UpdateKPForm(instance=kpInfo)
        return JsonResponse({'form': updateForm.as_p(), 'devMessage':'success'})


@role_required(allowed_roles=['manager',])
def addContact(request, id):

    if request.method == 'GET':
        form = ContactInfoForm(id)
        return JsonResponse({"form": form.as_p()})

    if request.method == 'POST':
       
        form = ContactInfoForm(id, request.POST)
        print(form.errors)
        if form.is_valid():
            contact = form.save(commit=False)
            try:
                contact.supplier = Supplier.objects.get(pk=id)
            except Supplier.DoesNotExist:
                message = "Does Not Exist: Can't link contact info because Supplier does not exist anymore."
                return JsonResponse({'devMessage': message, 'form': form.as_p()})
            kpName = form.cleaned_data['keyPersonnel']
            if kpName is None:
                contact.keyPersonnel = None
            else:
                kp = KeyPersonnel.objects.get(name=kpName)
                contact.keyPersonnel = kp

            contact.save()
            return JsonResponse({"form": form.as_p(), "devMessage": 'success'})

        return JsonResponse({"form": form.as_p(), "devMessage": 'Error'})


@role_required(allowed_roles=['manager',])
def deleteContact(request, id):

    if request.method == 'POST':
        password = request.POST.get("password", "")
        correct_password = check_password(password, request.user.password)

        if correct_password:
            con = None
            message = None
            delete_success = False
            try:
                con = ContactInfo.objects.get(pk=id) 
                con.delete()
                delete_success = True
                message = 'success'
            except RestrictedError:
                delete_success = False
                message = 'Restricted Error: A part of the system is using this Contact Info. You must delete those first.'
            except ContactInfo.DoesNotExist:
                delete_success = False
                message = 'Does Not Exist: Entry does not exist anymore, please refresh table.'
            return JsonResponse({'delete_success': delete_success, 'devMessage': message})
        else:
            return JsonResponse({'delete_success': 'False', 'devMessage': 'Wrong Password'})


@role_required(allowed_roles=['manager',])
@transaction.atomic
def updateContact(request, id, sid):

    if request.method == 'POST':
        
        try:
            instance = ContactInfo.objects.get(pk=id)
        except ContactInfo.DoesNotExist:
            message = 'Contact Does Not Exist. May have been deleted. Please Refresh Table'
            form = UpdateContactForm()
            return JsonResponse({'devMessage': message, 'form': form.as_p()})

        form = UpdateContactForm(request.POST, instance=instance)
        try:
            if form.is_valid():
                form.save()
                return JsonResponse({'devMessage': 'success', 'form': form.as_p()})
        except IntegrityError:
            ieError = "Some Integrity Error occured"
            return JsonResponse({"devMessage": ieError, 'form': form.as_p()})

        return JsonResponse({'devMessage': 'error', 'form': form.as_p()})

    if request.method == 'GET':
        try:
            inst = ContactInfo.objects.get(pk=id)
        except ContactInfo.DoesNotExist:
            updateForm = UpdateContactForm()
            return JsonResponse({'error': 'Entry Does Not Exist - Please refresh table', 'form': updateForm.as_p()})
            
        updateForm = UpdateContactForm(instance=inst)
        return JsonResponse({'form': updateForm.as_p()})
        

@role_required(allowed_roles=['manager',])
def loadTransactionsAjax(request, id):
    transactions = ItemTransactions.objects.filter(supplied_by=id)
    jsonTransactions = []
    for transaction in transactions:
        total = (transaction.quantity * transaction.price_per_piece)
        aTransaction = {'pk': transaction.pk,
                        'created_by': str(transaction.created_by),
                        'item': str(transaction.item),
                        'quantity': transaction.quantity,
                        'price_per_piece': str(transaction.price_per_piece),
                        'total': str(total),
                        'remarks': transaction.remarks,
                        'date': str(transaction.date)
                        }
        jsonTransactions.append(aTransaction)

    jsonTransactions = json.dumps(jsonTransactions, indent=4)
    return HttpResponse(jsonTransactions, content_type="application/json")


@role_required(allowed_roles=['manager',])
def deleteTransaction(request, id):
    return inventory_view.deleteTransaction(request=request, id=id)

@role_required(allowed_roles=['manager',])
@transaction.atomic
def addTransaction(request, id):

    if request.method == 'POST':

        form = ItemTransactionSupForm(request.POST, id)

        if form.is_valid():
            transaction = form.save(commit=False)
            try:
                supplier = Supplier.objects.get(pk=id)
            except Supplier.DoesNotExist:
                message = "Does Not Exist: Can't add transaction because Supplier does not exist anymore."
                return JsonResponse({'devMessage': message, 'form': form.as_p()})
            
            item = form.cleaned_data['item']
            transaction.supplied_by = supplier
            transaction.created_by = request.user
            transaction.save()
            # have to unpack when using get_or_create
            # transacts with ItemAvailability
            availItem, created = ItemAvailability.objects.get_or_create(
                item=item)
            availItem.total_available += transaction.quantity
            availItem.save()

            #sup = form['supplied_by'].value()
            #supClean = form.cleaned_data['supplied_by']
            #supName = supClean.name
            extraMessage = None
            if (KnownSuppliers.objects.filter(supplier=supplier, itemInfo=item).exists()) is False:

                KnownSuppliers.objects.create(supplier=supplier,
                                              itemInfo=item,
                                              price_per_piece=form.cleaned_data['price_per_piece'],
                                              moq=form.cleaned_data['quantity'],
                                              lead_time_days=0,
                                              remarks="Automatically Added By System.")

                extraMessage = 'Supplier: "' + supplier.name + '" was not linked with Known Suppliers Table.\n' + \
                    'System automatically linked Supplier: "' + supplier.name + '" to Known Suppliers table. \n' + \
                    'Please fix details in Known Supplier table if necessary.'

            return JsonResponse({"form": form.as_p(), "devMessage": 'success', 'extraMessage': extraMessage})

        return JsonResponse({"form": form.as_p(), "devMessage": 'Error'})


@role_required(allowed_roles=['manager',])
def loadLinkedItemsAjax(request, id):
    supplier = KnownSuppliers.objects.filter(supplier=id)  # join
    jsonItems = []
    for entry in supplier:

        discounts_json = []
        discounts = KnownDiscounts.objects.filter(
            knownSupplier=entry.pk)

        for eachDiscount in discounts:
            each = {
                'pk': eachDiscount.pk,
                'moq': eachDiscount.moq,
                'discount_rate': str(eachDiscount.discountRate)
            }
            discounts_json.append(each)

        entry = {'pk': entry.pk,
                 'item': entry.itemInfo.name,
                 'moq': entry.moq,
                 'price_per_piece': str(entry.price_per_piece),
                 'lead_time': entry.lead_time_days,
                 'remarks': entry.remarks,
                 'discounts': discounts_json
                 }
        jsonItems.append(entry)

    jsonItems = json.dumps(jsonItems, indent=4)

    return HttpResponse(jsonItems, content_type="application/json")


@role_required(allowed_roles=['manager',])
@transaction.atomic
def linkItemToSupplier(request, id):
    if request.method == 'GET':
        linkItemForm = LinkKnownItemToSupplierForm(
            instance=None, supplierPK=id)
        return JsonResponse({"form": linkItemForm.as_p()})

    if request.method == 'POST':

        form = LinkKnownItemToSupplierForm(id, request.POST)

        try:
            if form.is_valid():
                linkForm = form.save(commit=False)
                try:
                    supplier = Supplier.objects.get(pk=id)
                except Supplier.DoesNotExist:
                    message = "Does Not Exist: Can't link item because Supplier does not exist anymore."
                    return JsonResponse({'devMessage': message, 'form': form.as_p()})

                linkForm.supplier = supplier
                linkForm.save()
                linkItemForm = LinkKnownItemToSupplierForm(
                    instance=None, supplierPK=id)
                return JsonResponse({"form": linkItemForm.as_p(), "devMessage": 'success'})
        except IntegrityError:
            ieError = "Some Integrity Error occured"
            return JsonResponse({"devMessage": ieError, 'form': form.as_p()})

        return JsonResponse({"form": form.as_p(), "devMessage": 'Error'})


