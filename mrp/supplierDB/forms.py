from django.forms import ModelForm, Textarea
from .models import Supplier, KeyPersonnel, ContactInfo
from inventory.models import ItemTransactions, KnownSuppliers, ItemInfo
from django.forms.widgets import TextInput
from colorfield.widgets import ColorWidget
from phonenumber_field.widgets import PhoneNumberPrefixWidget
from phonenumber_field.formfields import PhoneNumberField
from django import forms
from datetime import date
import datetime

today = date.today()
minDate = datetime.datetime.strptime('19500101', '%Y%m%d').date()


class SupplierForm(ModelForm):
    class Meta:
        model = Supplier
        fields = ('name',
                  'tin',
                  'address',
                  'rating',
                  'sellerType',
                  'remarks',
                  )
        widgets = {
            'name': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Name'}),
            'tin': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Taxpayer Identification Number'}),
            'rating': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'Rating'}),
            'address': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Address', "rows": '4'}),
            'sellerType': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Type of seller'}),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),

        }
        labels = {
            "tin": "TIN",
            "sellerType": "Type of Seller"
        }


class KeyPersonnelForm(ModelForm):
    class Meta:
        model = KeyPersonnel
        fields = ('name',
                  'description',
                  'color_code',
                  'remarks',
                  )
        widgets = {
            'color_code': forms.CharField(widget=forms.TextInput(attrs={'id': 'colorPicker'})),
            'name': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Name'}),
            'description': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
        }

    def __init__(self, *args, **kwargs):
        super(KeyPersonnelForm, self).__init__(*args, **kwargs)
        self.fields['color_code'] = forms.CharField(
            widget=forms.TextInput(attrs={'id': 'colorPicker', 'class': 'input'}), required=False)


class UpdateKPForm(ModelForm):
    class Meta:
        model = KeyPersonnel
        fields = ('name',
                  'description',
                  'color_code',
                  'remarks',
                  )
        widgets = {
            # 'color_code': forms.CharField(widget=forms.TextInput(attrs={'id': 'colorPicker2'})),
            'name': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Name'}),
            'description': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
        }

    def __init__(self, *args, **kwargs):
        super(UpdateKPForm, self).__init__(*args, **kwargs)
        self.fields['color_code'] = forms.CharField(
            widget=forms.TextInput(attrs={'id': 'colorPicker2', 'class': 'input'}), required=False)


class ContactInfoForm(ModelForm):
    class Meta:
        model = ContactInfo

        fields = ('keyPersonnel',
                  'phone_number',
                  'email_address',
                  'description',
                  'remarks',
                  )
        widgets = {
            'phone_number': PhoneNumberPrefixWidget(initial='PH'),
            'email_address': forms.EmailInput(
                attrs={'class': "input", 'type': 'email', 'placeholder': 'e-mail'}),
            'description': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
        }

    def __init__(self, supplierPK, *args, **kwargs):
        super(ContactInfoForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['keyPersonnel'].queryset = KeyPersonnel.objects.filter(
                supplier=supplierPK).order_by('name')


class UpdateContactForm(ModelForm):
    class Meta:
        model = ContactInfo

        fields = (
            'phone_number',
            'email_address',
            'description',
            'remarks',
        )
        widgets = {
            'phone_number': PhoneNumberPrefixWidget(initial='PH'),
            'email_address': forms.EmailInput(
                attrs={'class': "input", 'type': 'email', 'placeholder': 'e-mail'}),
            'description': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
        }


class ItemTransactionSupForm(ModelForm):
    class Meta:
        model = ItemTransactions
        fields = (
            'item',
            'quantity',
            'price_per_piece',
            'date',
            'remarks')

        widgets = {
            'date': forms.DateInput(format=('%Y/%m/%d'),
                                    attrs={'class': 'datepicker input', 'type': 'date',
                                           'min': str(minDate), 'max': str(today)}
                                    ),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
            'price_per_piece': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'SRP', 'step': "0.01"}),
            'quantity': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'Quantity'}),

        }

    def clean_date(self):
        date = self.cleaned_data['date']
        if date > today or date < minDate:
            raise forms.ValidationError(
                'Inputted date is outside of allowed range.')
        return date


class LinkKnownItemToSupplierForm(ModelForm):
    class Meta:
        model = KnownSuppliers
        fields = (
            'itemInfo',
            'price_per_piece',
            'moq',
            'lead_time_days',
            'remarks')

        widgets = {

            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
            'price_per_piece': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'SRP', 'step': "0.01"}),
            'moq': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'Minimum Order Quantity'}),
            'lead_time_days': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'Lead Time in Days'}),

        }

    def __init__(self, supplierPK, *args, **kwargs):
        super(LinkKnownItemToSupplierForm, self).__init__(*args, **kwargs)
        if self.instance:
            knownItem = KnownSuppliers.objects.filter(
                supplier=supplierPK).values_list('itemInfo_id', flat=True)  # know the know items of supplier
            itemPKList = list(knownItem)  # list it
            # exclude in form if already there
            listItem = ItemInfo.objects.exclude(pk__in=itemPKList)
            self.fields['itemInfo'].queryset = listItem.order_by('name')
