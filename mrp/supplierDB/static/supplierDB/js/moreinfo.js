/*==================================================================*/
// delete Supplier
let deleteSupplierForm = $("#delete-supplier-form");
let deleteSupplierFormTok = $("#delete-item-form input[name='csrfmiddlewaretoken']").clone();
let deleteSupplierFormPost = "{% url 'deleteSupplierInfo' id=supplierInfo.pk %}";
let deleteSupplierFormSuccess = "Supplier Deleted Successfully";
let deleteSupplierFormError = "Error Occured";
function deleteSupplierExtra() {
    window.open('', '_self').close();
}

deleteSupplierForm.submit(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: deleteSupplierFormPost,
        data: deleteSupplierForm.serialize(),
        dataType: "json",
        success: function (data) {
            if (data.devMessage == 'success') {
                alert(deleteSupplierFormSuccess);
                deleteSupplierExtra();
            } else {
                alert(data.devMessage);
                deleteSupplierForm.trigger('reset');
            }
        },
        error: function (xhr, status, error) {

        },
    });
});
/*==================================================================*/

//update Supplier
let updateSupplierForm = $("#update-supplier-form");
let updateSupplierFormTok = $("#update-supplier-form input[name='csrfmiddlewaretoken']").clone();
let updateSupplierFormPost = "{% url 'updateSupplierInfo' id=supplierInfo.pk %}";
let updateSupplierFormSuccess = "Supplier Updated Successfully";
let updateSupplierFormError = "Error Occured";
function updateSupplierExtra() {
    location.reload();
}

updateSupplierForm.submit(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: updateSupplierFormPost,
        data: updateSupplierForm.serialize(),
        dataType: "json",
        success: function (data) {
            if (data.devMessage == 'success') {
                alert(updateSupplierFormSuccess);
                updateSupplierExtra();
            } else {
                alert(data.devMessage);
                rebuildForm(updateSupplierForm, data.form, updateSupplierFormTok);
            }
        },
        error: function (xhr, status, error) {

        },
    });
});

/*==================================================================*/
let contFormRefVar = "{% url 'addContact' id=supplierInfo.pk %}"
//refreshes contact form
/*==================================================================*/
//add kp
let addKPForm = $("#add-kp-form");
let addKPFormTok = $("#add-kp-form input[name='csrfmiddlewaretoken']").clone();
let addKPFormPost = "{% url 'addKeyPersonnel' id=supplierInfo.pk %}";
let addKPFormRef = '{% url "loadKPAjax" id=supplierInfo.pk %}';
let addKPFormSuccess = "Personnel Added Successfully"
let addKPFormError = "Error Occured"
function addKPExtra() {
    addKPForm.trigger('reset');
    rebuildContactForm(contFormRefVar);
}

addKPForm.submit(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: addKPFormPost,
        data: addKPForm.serialize(),
        dataType: "json",
        success: function (data) {
            if (data.devMessage == 'success') {
                alert(addKPFormSuccess);
                refreshTable(kpTable, addKPFormRef); //in my_helpers.js
                addKPForm.trigger('reset');
                addKPExtra();
            } else {
                alert(data.devMessage);
            }
        },
        error: function (data) {
        }
    });
});

/*==================================================================*/
//delete kp

function deleteKPurl(url) {
    $('#delete-kp-form > p > input').val('');
    deleteKPForm.attr('action', url);
    $('#delete-kp-modal').addClass("is-active");
}

function delKPExtra() {
    deleteKPForm.trigger('reset');
    rebuildContactForm(contFormRefVar);
    $('.modal').removeClass("is-active");
}

let deleteKPForm = $("#delete-kp-form");
let deleteKPFormTok = $("#delete-kp-form input[name='csrfmiddlewaretoken']").clone();
//let deleteKPFormPost = deleteKPForm.attr('action');
let deleteKPFormRef = '{% url "loadKPAjax" id=supplierInfo.pk %}';
let deleteKPFormSuccess = "Personnel Deleted Successfully";
let deleteKPFormError = "Error Occured";

deleteKPForm.submit(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: deleteKPForm.attr('action'),
        data: deleteKPForm.serialize(),
        dataType: "json",
        success: function (data) {
            if (data.devMessage == 'success') {
                alert(deleteKPFormSuccess);
                refreshTable(kpTable, deleteKPFormRef);
                delKPExtra();
            } else {
                alert(data.devMessage);
                deleteKPForm.trigger('reset');
            }
        },
        error: function (data) {

        }
    });
});


/*==================================================================*/
//update kp

function updateKPurl(url) {
    updateKPForm.attr('action', url);
}

function updateKPExtra() {
    location.reload();
}

let updateKPForm = $("#update-kp-form");
let updateKPFormTok = $("#update-kp-form input[name='csrfmiddlewaretoken']").clone();
let updateKPFormPost = $("#update-kp-form").attr('action');
let updateKPFormRef = '{% url "loadKPAjax" id=supplierInfo.pk %}';
let updateKPFormSuccess = "Personnel Updated Successfully";
let updateKPFormError = "Error Occured";

updateKPForm.submit(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: updateKPForm.attr('action'),
        data: updateKPForm.serialize(),
        dataType: "json",
        success: function (data) {
            if (data.devMessage == 'success') {
                alert(updateKPFormSuccess);
                refreshTable(kpTable, updateKPFormRef);
                updateKPExtra();
            } else {
                alert(data.devMessage);
                rebuildForm(updateKPForm, data.form, updateKPFormTok);
            }
        },
        error: function (data) {

        }
    });
});


/*==================================================================*/

  //add contact
  let addContactForm = $("#add-contact-form");
  let addContactFormTok = $("#add-contact-form input[name='csrfmiddlewaretoken']").clone();
  let addContactFormPost = "{% url 'addContact' id=supplierInfo.pk %}";
  let addContactFormRef = '{% url "loadContactAjax" id=supplierInfo.pk %}';
  let addContactFormSuccess = "Contact Info Added Successfully";
  let addContactFormError = "Error Occured";

  function toggleAddContact() {
    addContactForm.trigger('reset');
    $("#id_phone_number_0").val('+63');
    rebuildContactForm(contFormRefVar);
  }

  addContactForm.submit(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: addContactFormPost,
        data: addContactForm.serialize(),
        dataType: "json",
        success: function (data) {
            if (data.devMessage == 'success') {
                alert(addContactFormSuccess);
                refreshTable(conTable, tableURL); //in my_helpers.js
                rebuildForm(addContactForm, data.form, addContactFormTok);
                toggleAddContact();
            } else {
                alert(data.devMessage);
                rebuildForm(addContactForm, data.form, addContactFormTok);
            }
        },
        error: function (xhr, status, error) {

        },
    });
});


 

  /*==================================================================*/
  //delete contact

  function deleteContact(url) {
    $('#delete-contact-form > p > input').val('');
    deleteConForm.attr('action', url);
    $('#delete-contact-modal').addClass("is-active");
  }

  function delConExtra() {
    deleteConForm.trigger('reset');
    rebuildContactForm(contFormRefVar);
    $('.modal').removeClass("is-active");
  }

  let deleteConForm = $("#delete-contact-form");
  let deleteConFormTok = $("#delete-contact-form input[name='csrfmiddlewaretoken']").clone();
  //let deleteKPFormPost = deleteKPForm.attr('action');
  let deleteConFormRef = '{% url "loadContactAjax" id=supplierInfo.pk %}';
  let deleteConFormSuccess = "Contact Deleted Successfully";
  let deleteConFormError = "Error Occured";

  deleteConForm.submit(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: deleteConForm.attr('action'),
        data: deleteConForm.serialize(),
        dataType: "json",
        success: function (data) {
            if (data.devMessage == 'success') {
                alert(deleteConFormSuccess);
                refreshTable(conTable, deleteConFormRef);
                delConExtra();
            } else {
                alert(data.devMessage);
                deleteConForm.trigger('reset');
            }
        },
        error: function (data) {

        }
    });
});

  /*==================================================================*/
  //update contact

  function updateContact(url) {
    updateConForm.attr('action', url);
    let tok = $("#update-contact-form input[name='csrfmiddlewaretoken']").clone();
    updateForm(updateConForm, tok, url);
    $('#update-contact-modal').addClass("is-active");
  }

  function updateConExtra() {
    $('.modal').removeClass("is-active");
  }

  let updateConForm = $("#update-contact-form");
  let updateConFormTok = $("#update-contact-form input[name='csrfmiddlewaretoken']").clone();
  let updateConFormPost = updateConForm.attr('action');
  let updateConFormRef = '{% url "loadContactAjax" id=supplierInfo.pk %}';
  let updateConFormSuccess = "Contact Updated Successfully";
  let updateConFormError = "Error Occured";
  
  updateConForm.submit(function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: updateConFormPost,
        data: updateConForm.serialize(),
        dataType: "json",
        success: function (data) {
            if (data.devMessage == 'success') {
                alert(updateConFormSuccess);
                refreshTable(conTable, updateConFormRef); //in my_helpers.js
                rebuildForm(updateConForm, data.form, updateConFormTok);
                updateConExtra();
            } else {
                alert(data.devMessage);
                rebuildForm(updateConForm, data.form, updateConFormTok);
            }
        },
        error: function (xhr, status, error) {

        },
    });
});

