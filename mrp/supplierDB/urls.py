from django.urls import path
from . import views

urlpatterns = [
    path('', views.supplierPage, name='supplier_page'),
    path('load_suppliers_ajax/', views.loadSuppliersAjax, name='loadSuppliersAjax'),
    path('viewmoreinfo/<int:pk>/', views.viewMoreSupplierInfo, name='viewMoreSupplierInfo'),
    path('delete/info/<int:id>', views.deleteSupplierInfo, name='deleteSupplierInfo'),
    path('update/info/<int:id>', views.updateSupplierInfo, name='updateSupplierInfo'),
    path('add/kp/<int:id>', views.addKeyPersonnel, name='addKeyPersonnel'),
    path('add/contact/<int:id>', views.addContact, name='addContact'),
    path('load_kp_ajax/<int:id>', views.loadKeyPersonnelAjax, name='loadKPAjax'),
    path('load_contact_ajax/<int:id>', views.loadContactInfoAjax, name='loadContactAjax'),
    path('delete/kp/<int:id>', views.deleteKP, name='deleteKP'),
    path('delete/contact/<int:id>', views.deleteContact, name='deleteContact'),
    path('update/kp/<int:id>', views.updateKPInfo, name='updateKeyPersonnel'),
    path('update/contact/<int:sid>/<int:id>', views.updateContact, name='updateContact'),
    path('load_transactions_ajax/<int:id>', views.loadTransactionsAjax, name='loadTransactionsForSupAjax'),
    path('delete/transaction/<int:id>', views.deleteTransaction, name='deleteTransactionForSup'),
    path('add/transaction/<int:id>', views.addTransaction, name='addTransactionForSup'),
    path('load_link_items_ajax/<int:id>', views.loadLinkedItemsAjax, name='loadLinkItems'),
    path('link/item/<int:id>', views.linkItemToSupplier, name='linkItemToSupplier'),
    path('load_kp_and_contact_ajax/<int:id>', views.loadKPAndConAjax, name='loadKPAndConAjax'),
    

]