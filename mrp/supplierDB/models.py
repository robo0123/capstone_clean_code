from django.db import models

# added
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from simple_history.models import HistoricalRecords
from phonenumber_field.modelfields import PhoneNumberField
from colorfield.fields import ColorField
# added

# Create your models here.


class Supplier(models.Model):
    name = models.CharField(max_length=140, blank=False,
                            null=False, unique=True)
    tin = models.CharField(max_length=70, blank=True, null=True)
    address = models.TextField(blank=False, null=False)
    rating = models.PositiveIntegerField(blank=True, null=True, default=0,
                                         validators=[MaxValueValidator(
                                             10), MinValueValidator(0)]
                                         )
    sellerType = models.CharField(max_length=25, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)
    #close_down = models.BooleanField(blank=False, null=False, default=False)
    history = HistoricalRecords()

    def __str__(self):
        return self.name


class KeyPersonnel(models.Model):
    supplier = models.ForeignKey(
        Supplier, on_delete=models.RESTRICT, blank=False, null=False)
    name = models.CharField(max_length=140, blank=False, null=False)
    description = models.TextField(blank=True, null=True)
    color_code = ColorField(blank=True, null=True, default='#000000')
    remarks = models.TextField(blank=True, null=True)
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['supplier', 'name'],
                name='unique kp name per supplier'
            )
        ]

    def __str__(self):
        return self.name


class ContactInfo(models.Model):
    supplier = models.ForeignKey(
        Supplier, on_delete=models.RESTRICT, blank=False, null=False)
    keyPersonnel = models.ForeignKey(
        KeyPersonnel, on_delete=models.RESTRICT, blank=True, null=True)
    phone_number = PhoneNumberField(blank=True, null=True)
    email_address = models.EmailField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.supplier.name
