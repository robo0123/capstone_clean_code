from django.apps import AppConfig


class SupplierdbConfig(AppConfig):
    name = 'supplierDB'
