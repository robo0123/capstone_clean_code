"""mrp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('bosspanel/', admin.site.urls),
    path('inventory/', include('inventory.urls')),  # goes to urls.py in inventory app
    path('', include('accounts.urls')),  # goes to urls.py in accounts app
    path('supplier/', include('supplierDB.urls')), # goes to urls.py in supplierDB app
    path('construction/', include('construction_journal.urls')),
    path('accountant/', include('accountant.urls')),
    path('crud_history/', include('crud_history.urls')),
]
