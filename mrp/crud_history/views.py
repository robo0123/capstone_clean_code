from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
from inventory.models import ItemInfo, ItemTransactions, KnownSuppliers, KnownDiscounts
from supplierDB.models import Supplier, ContactInfo, KeyPersonnel
from construction_journal.models import ConstructionProject, ItemTransactionTracker
from accountant.models import BankAccounts, PaymentDueDate
from django.contrib.auth import get_user_model
import datetime
import pytz
from django.template import Context
from django.core.exceptions import ObjectDoesNotExist
from accounts.decorators import role_required

# Create your views here.
user = get_user_model()
local_tz = pytz.timezone('Asia/Manila')


@role_required(allowed_roles=[])
def deletedInventory(request):
    return render(request, 'crud_history/overview/deleted_inventory.html')


@role_required(allowed_roles=[])
def loadDeletedItemInfo(request):

    allDeleted = ItemInfo.history.filter(history_type='-')
    jsonDeleted = []
    for item in allDeleted:

        dUser = user.objects.get(pk=item.history_user_id)
        date = local_tz.normalize(item.history_date)

        anItem = {'pk': item.pk,
                  'name': item.name,
                  'date': date.strftime("%Y-%m-%d %H:%M:%S"),
                  'user': dUser.username
                  }
        jsonDeleted.append(anItem)

    jsonDeleted = json.dumps(jsonDeleted, indent=4)
    return HttpResponse(jsonDeleted, content_type="application/json")


@role_required(allowed_roles=[])
def moreItemInfo(request, pk):

    moreInfo = ItemInfo.history.get(pk=pk, history_type='-')
    dUser = user.objects.get(pk=moreInfo.history_user_id)
    date = local_tz.normalize(moreInfo.history_date)

    context = {'username': dUser.username,
               'info': moreInfo,
               'date': date.strftime("%Y-%m-%d %H:%M:%S")
               }

    return render(request, 'crud_history/detail/item_info.html', context)


@role_required(allowed_roles=[])
def loadDeletedItemTransaction(request):

    allDeleted = ItemTransactions.history.filter(history_type='-')
    jsonDeleted = []
    for transaction in allDeleted:

        dUser = user.objects.get(pk=transaction.history_user_id)
        date = local_tz.normalize(transaction.history_date)
        item = ItemInfo.history.filter(id=transaction.item_id).latest()
        supplier = Supplier.history.filter(
            id=transaction.supplied_by_id).latest()
        transDate = transaction.date
        pricePerPiece = transaction.price_per_piece
        quantity = transaction.quantity

        aTrans = {'pk': transaction.pk,
                  'item': item.name,
                  'supplied_by': supplier.name,
                  'date': date.strftime("%Y-%m-%d %H:%M:%S"),
                  'transDate': str(transDate),
                  'pricePerPiece': str(pricePerPiece),
                  'quantity': quantity,
                  'user': dUser.username
                  }
        jsonDeleted.append(aTrans)

    jsonDeleted = json.dumps(jsonDeleted, indent=4)
    return HttpResponse(jsonDeleted, content_type="application/json")


@role_required(allowed_roles=[])
def moreItemTransInfo(request, pk):

    moreInfo = ItemTransactions.history.get(pk=pk, history_type='-')
    dUser = user.objects.get(pk=moreInfo.history_user_id)
    date = local_tz.normalize(moreInfo.history_date)
    item = ItemInfo.history.filter(id=moreInfo.item_id).latest()
    supplier = Supplier.history.filter(id=moreInfo.supplied_by_id).latest()

    context = {'username': dUser.username,
               'info': moreInfo,
               'date': date.strftime("%Y-%m-%d %H:%M:%S"),
               'itemName': item.name,
               'supplier': supplier.name,
               }

    return render(request, 'crud_history/detail/item_trans.html', context)


@role_required(allowed_roles=[])
def loadDeletedKS(request):

    allDeleted = KnownSuppliers.history.filter(history_type='-')
    jsonDeleted = []
    for ks in allDeleted:

        dUser = user.objects.get(pk=ks.history_user_id)
        date = local_tz.normalize(ks.history_date)
        item = ItemInfo.history.filter(id=ks.itemInfo_id).latest()
        supplier = Supplier.history.filter(id=ks.supplier_id).latest()

        aKS = {'pk': ks.pk,
               'item': item.name,
               'supplier': supplier.name,
               'moq': ks.moq,
               'price_per_piece': str(ks.price_per_piece),
               'lead_time': ks.lead_time_days,
               'rating': supplier.rating,
               'date': date.strftime("%Y-%m-%d %H:%M:%S"),
               'user': dUser.username
               }
        jsonDeleted.append(aKS)

    jsonDeleted = json.dumps(jsonDeleted, indent=4)
    return HttpResponse(jsonDeleted, content_type="application/json")


@role_required(allowed_roles=[])
def moreKSInfo(request, pk):

    moreInfo = KnownSuppliers.history.get(pk=pk, history_type='-')
    dUser = user.objects.get(pk=moreInfo.history_user_id)
    date = local_tz.normalize(moreInfo.history_date)
    item = ItemInfo.history.filter(id=moreInfo.itemInfo_id).latest()
    supplier = Supplier.history.filter(id=moreInfo.supplier_id).latest()

    context = {'username': dUser.username,
               'info': moreInfo,
               'date': date.strftime("%Y-%m-%d %H:%M:%S"),
               'itemName': item.name,
               'supplier': supplier.name,
               'supplier_rating': supplier.rating,
               }

    return render(request, 'crud_history/detail/known_suppliers.html', context)


@role_required(allowed_roles=[])
def loadDeletedDiscounts(request):

    allDeleted = KnownDiscounts.history.filter(history_type='-')
    jsonDeleted = []
    for discounts in allDeleted:

        dUser = user.objects.get(pk=discounts.history_user_id)
        date = local_tz.normalize(discounts.history_date)

        ks = KnownSuppliers.history.filter(
            id=discounts.knownSupplier_id).latest()
        supplier = Supplier.history.filter(id=ks.supplier_id).latest()
        supName = supplier.name

        item = ItemInfo.history.filter(id=ks.itemInfo_id).latest()
        itemName = item.name

        aDiscount = {'pk': discounts.pk,
                     'item': itemName,
                     'moq': discounts.moq,
                     'discountRate': discounts.discountRate,
                     'supplier': supName,
                     'date': date.strftime("%Y-%m-%d %H:%M:%S"),
                     'user': dUser.username
                     }
        jsonDeleted.append(aDiscount)

    jsonDeleted = json.dumps(jsonDeleted, indent=4)
    return HttpResponse(jsonDeleted, content_type="application/json")


@role_required(allowed_roles=[])
def deletedSupplier(request):
    return render(request, 'crud_history/overview/deleted_supplier.html')


@role_required(allowed_roles=[])
def loadDeletedSupplier(request):

    allDeleted = Supplier.history.filter(history_type='-')
    jsonDeleted = []
    for supplier in allDeleted:

        dUser = user.objects.get(pk=supplier.history_user_id)
        date = local_tz.normalize(supplier.history_date)

        aSupplier = {'pk': supplier.pk,
                     'name': supplier.name,
                     'tin': supplier.tin,
                     'sellerType': supplier.sellerType,
                     'address': supplier.address,
                     'date': date.strftime("%Y-%m-%d %H:%M:%S"),
                     'user': dUser.username
                     }
        jsonDeleted.append(aSupplier)

    jsonDeleted = json.dumps(jsonDeleted, indent=4)
    return HttpResponse(jsonDeleted, content_type="application/json")


@role_required(allowed_roles=[])
def moreSupplierInfo(request, pk):

    moreInfo = Supplier.history.get(pk=pk, history_type='-')
    dUser = user.objects.get(pk=moreInfo.history_user_id)
    date = local_tz.normalize(moreInfo.history_date)

    context = {'username': dUser.username,
               'info': moreInfo,
               'date': date.strftime("%Y-%m-%d %H:%M:%S")
               }

    return render(request, 'crud_history/detail/supplier.html', context)


@role_required(allowed_roles=[])
def loadDeletedContact(request):

    allDeleted = ContactInfo.history.filter(history_type='-')
    jsonDeleted = []
    for contactInfo in allDeleted:

        dUser = user.objects.get(pk=contactInfo.history_user_id)
        date = local_tz.normalize(contactInfo.history_date)

        supplier = Supplier.history.filter(id=contactInfo.supplier_id).latest()
        supName = supplier.name

        kpName = None
        try:
            kp = KeyPersonnel.history.filter(
                id=contactInfo.keyPersonnel_id).latest()
            kpName = kp.name
        except KeyPersonnel.DoesNotExist:
            kpName = None
        except ObjectDoesNotExist:
            kpName = None

        aContact = {'pk': contactInfo.pk,
                    'supplier': supName,
                    'keyPersonnel': kpName,
                    'phone_number': str(contactInfo.phone_number),
                    'email_address': contactInfo.email_address,
                    'date': date.strftime("%Y-%m-%d %H:%M:%S"),
                    'user': dUser.username
                    }
        jsonDeleted.append(aContact)

    jsonDeleted = json.dumps(jsonDeleted, indent=4)
    return HttpResponse(jsonDeleted, content_type="application/json")


@role_required(allowed_roles=[])
def moreContactInfo(request, pk):

    moreInfo = ContactInfo.history.get(pk=pk, history_type='-')
    dUser = user.objects.get(pk=moreInfo.history_user_id)
    date = local_tz.normalize(moreInfo.history_date)

    supplier = Supplier.history.filter(id=moreInfo.supplier_id).latest()
    supName = supplier.name

    kpName = None
    try:
        kp = KeyPersonnel.history.filter(
            id=moreInfo.keyPersonnel_id).latest()
        kpName = kp.name
    except KeyPersonnel.DoesNotExist:
        kpName = None
    except ObjectDoesNotExist:
        kpName = None

    context = {'username': dUser.username,
               'info': moreInfo,
               'date': date.strftime("%Y-%m-%d %H:%M:%S"),
               'supName': supName,
               'kpName': kpName
               }

    return render(request, 'crud_history/detail/contact.html', context)


@role_required(allowed_roles=[])
def loadDeletedKP(request):

    allDeleted = KeyPersonnel.history.filter(history_type='-')
    jsonDeleted = []
    for kp in allDeleted:

        supplier = Supplier.history.filter(id=kp.supplier_id).latest()
        supName = supplier.name

        dUser = user.objects.get(pk=kp.history_user_id)
        date = local_tz.normalize(kp.history_date)

        kp = {'pk': kp.pk,
              'name': kp.name,
              'supplier': supName,
              'date': date.strftime("%Y-%m-%d %H:%M:%S"),
              'user': dUser.username
              }
        jsonDeleted.append(kp)

    jsonDeleted = json.dumps(jsonDeleted, indent=4)
    return HttpResponse(jsonDeleted, content_type="application/json")


@role_required(allowed_roles=[])
def moreKPInfo(request, pk):

    moreInfo = KeyPersonnel.history.get(pk=pk, history_type='-')
    dUser = user.objects.get(pk=moreInfo.history_user_id)
    date = local_tz.normalize(moreInfo.history_date)

    supplier = Supplier.history.filter(id=moreInfo.supplier_id).latest()
    supName = supplier.name

    context = {'username': dUser.username,
               'info': moreInfo,
               'date': date.strftime("%Y-%m-%d %H:%M:%S"),
               'supName': supName,
               }

    return render(request, 'crud_history/detail/kp.html', context)


@role_required(allowed_roles=[])
def deletedConstruction(request):
    return render(request, 'crud_history/overview/deleted_construction.html')


@role_required(allowed_roles=[])
def loadDeletedConsProj(request):

    allDeleted = ConstructionProject.history.filter(history_type='-')
    jsonDeleted = []
    for project in allDeleted:

        dUser = user.objects.get(pk=project.history_user_id)
        date = local_tz.normalize(project.history_date)

        aProj = {'pk': project.pk,
                 'job_order_num': project.job_order_num,
                 'project': project.project,
                 'date': date.strftime("%Y-%m-%d %H:%M:%S"),
                 'user': dUser.username
                 }
        jsonDeleted.append(aProj)

    jsonDeleted = json.dumps(jsonDeleted, indent=4)
    return HttpResponse(jsonDeleted, content_type="application/json")


@role_required(allowed_roles=[])
def moreConsProjInfo(request, pk):

    moreInfo = ConstructionProject.history.get(pk=pk, history_type='-')
    dUser = user.objects.get(pk=moreInfo.history_user_id)
    date = local_tz.normalize(moreInfo.history_date)

    context = {'username': dUser.username,
               'info': moreInfo,
               'date': date.strftime("%Y-%m-%d %H:%M:%S")
               }

    return render(request, 'crud_history/detail/cons_proj.html', context)


@role_required(allowed_roles=[])
def loadDeletedTracked(request):

    allDeleted = ItemTransactionTracker.history.all()
    jsonDeleted = []
    for field in allDeleted:

        constProj = ConstructionProject.history.filter(
            id=field.constProject_id).latest()
        transaction = ItemTransactions.history.filter(
            id=field.itemTrans_id).latest()

        dUser = user.objects.get(pk=field.history_user_id)
        date = local_tz.normalize(field.history_date)

        historyType = field.history_type
        historyTypeStr = None
        if historyType == '+':
            historyTypeStr = 'created'
        elif historyType == '~':
            historyTypeStr = 'edited'
        elif historyType == '-':
            historyTypeStr = 'deleted'
        else:
            historyTypeStr = 'error'

        aProj = {'pk': field.pk,
                 'job_order_num': str(constProj.job_order_num),
                 'item': str(transaction.item),
                 'transDate': str(transaction.date),
                 'quantityTracked': field.quantity,
                 'editType': historyTypeStr,
                 'date': date.strftime("%Y-%m-%d %H:%M:%S"),
                 'user': dUser.username
                 }
        jsonDeleted.append(aProj)

    jsonDeleted = json.dumps(jsonDeleted, indent=4)
    return HttpResponse(jsonDeleted, content_type="application/json")


@role_required(allowed_roles=[])
def deletedAccounting(request):
    return render(request, 'crud_history/overview/deleted_accounting.html')


@role_required(allowed_roles=[])
def loadDeletedBankAccts(request):

    allDeleted = BankAccounts.history.filter(history_type='-')
    jsonDeleted = []
    for bankAcct in allDeleted:

        dUser = user.objects.get(pk=bankAcct.history_user_id)
        date = local_tz.normalize(bankAcct.history_date)

        supplier = Supplier.history.filter(
            id=bankAcct.supplier_id).latest()

        anAcct = {'pk': bankAcct.pk,
                  'bank': bankAcct.bank,
                  'branch': bankAcct.branch,
                  'accountName': bankAcct.accountName,
                  'accountNum': bankAcct.accountNum,
                  'accountType': bankAcct.accountType,
                  'supplier': supplier.name,
                  'date': date.strftime("%Y-%m-%d %H:%M:%S"),
                  'user': dUser.username
                  }
        jsonDeleted.append(anAcct)

    jsonDeleted = json.dumps(jsonDeleted, indent=4)
    return HttpResponse(jsonDeleted, content_type="application/json")


@role_required(allowed_roles=[])
def moreBanksInfo(request, pk):

    moreInfo = BankAccounts.history.get(pk=pk, history_type='-')
    dUser = user.objects.get(pk=moreInfo.history_user_id)
    date = local_tz.normalize(moreInfo.history_date)
    supplier = Supplier.history.filter(id=moreInfo.supplier_id).latest()

    context = {'username': dUser.username,
               'info': moreInfo,
               'supplier': supplier.name,
               'date': date.strftime("%Y-%m-%d %H:%M:%S")
               }

    return render(request, 'crud_history/detail/bank_accounts.html', context)


@role_required(allowed_roles=[])
def loadDeletedDueDates(request):

    allDeleted = PaymentDueDate.history.filter(history_type='-')
    jsonDeleted = []
    for dues in allDeleted:

        dUser = user.objects.get(pk=dues.history_user_id)
        date = local_tz.normalize(dues.history_date)
        bankAcct = BankAccounts.history.filter(id=dues.bankAcct_id).latest()
        supplier = Supplier.history.filter(id=bankAcct.supplier_id).latest()

        '''
        supplier = Supplier.history.filter(
            id=dues.supplier_id).latest()'''

        aDue = {'pk': dues.pk,
                'supplier': supplier.name,
                'acctNum': bankAcct.accountNum,
                'amount': str(dues.amount),
                'dueDate': str(dues.dueDate),
                'date': date.strftime("%Y-%m-%d %H:%M:%S"),
                'user': dUser.username
                }
        jsonDeleted.append(aDue)

    jsonDeleted = json.dumps(jsonDeleted, indent=4)
    return HttpResponse(jsonDeleted, content_type="application/json")

@role_required(allowed_roles=[])
def moreDuesInfo(request, pk):

    moreInfo = PaymentDueDate.history.get(pk=pk, history_type='-')
    dUser = user.objects.get(pk=moreInfo.history_user_id)
    date = local_tz.normalize(moreInfo.history_date)
    bankAcct = BankAccounts.history.filter(id=moreInfo.bankAcct_id).latest()
    supplier = Supplier.history.filter(id=bankAcct.supplier_id).latest()

    context = {'username': dUser.username,
               'info': moreInfo,
               'supplier': supplier.name,
               'acctNum':bankAcct.accountNum,
               'date': date.strftime("%Y-%m-%d %H:%M:%S")
               }

    return render(request, 'crud_history/detail/due_dates.html', context)