from django.apps import AppConfig


class CrudHistoryConfig(AppConfig):
    name = 'crud_history'
