from django.urls import path
from . import views

urlpatterns = [

    path('deleted/inventory/', views.deletedInventory, name='deletedInventory'),
    path('load_deleted/item_info/', views.loadDeletedItemInfo, name='loadDeletedItemInfo'),
    path('load_deleted/item_trans/', views.loadDeletedItemTransaction, name='loadDeletedItemTransaction'),
    path('load_deleted/ks/', views.loadDeletedKS, name='loadDeletedKS'),
    path('load_deleted/kd/', views.loadDeletedDiscounts, name='loadDeletedDiscounts'),
    path('deleted/more_info/<int:pk>/', views.moreItemInfo, name='moreItemInfo'),
    path('deleted/more_info_trans/<int:pk>/', views.moreItemTransInfo, name='moreItemTransInfo'),
    path('deleted/more_info_ks/<int:pk>/', views.moreKSInfo, name='moreKSInfo'),

    path('deleted/supplier/', views.deletedSupplier, name='deletedSupplier'),
    path('load_deleted/supplier/', views.loadDeletedSupplier, name='loadDeletedSupplier'),
    path('load_deleted/contact/', views.loadDeletedContact, name='loadDeletedContact'),
    path('load_deleted/kp/', views.loadDeletedKP, name='loadDeletedKP'),
    path('deleted/more_info_supplier/<int:pk>/', views.moreSupplierInfo, name='moreSupplierInfo'),
    path('deleted/more_info_contact/<int:pk>/', views.moreContactInfo, name='moreContactInfo'),
    path('deleted/more_info_kp/<int:pk>/', views.moreKPInfo, name='moreKPInfo'),

    path('deleted/construction/', views.deletedConstruction, name='deletedConstruction'),
    path('load_deleted/cons_proj/', views.loadDeletedConsProj, name='loadDeletedConsProj'),
    path('load_deleted/tracked/', views.loadDeletedTracked, name='loadDeletedTracked'),
    path('deleted/more_info_cons_proj/<int:pk>/', views.moreConsProjInfo, name='moreConsProjInfo'),

    path('deleted/accounting/', views.deletedAccounting, name='deletedAccounting'),
    path('load_deleted/bank_acct/', views.loadDeletedBankAccts, name='loadDeletedBankAccts'),
    path('load_deleted/dues/', views.loadDeletedDueDates, name='loadDeletedDueDates'),
    path('deleted/more_info_bankAccts/<int:pk>/', views.moreBanksInfo, name='moreBanksInfo'),
    path('deleted/more_info_dues/<int:pk>/', views.moreDuesInfo, name='moreDuesInfo'),
    
    

]
