from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from .models import BankAccounts, PaymentDueDate

# Register your models here.

class ROAHAdmin(SimpleHistoryAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        pass

    def delete_model(self, request, obj):
        pass

    def save_related(self, request, form, formsets, change):
        pass

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save': False,
            'show_save_and_continue': False,
            'show_delete': False
        })
        return super().render_change_form(request, context, add, change, form_url, obj)


class BankAccountAdmin(ROAHAdmin):
    readonly_fields = ('bank', 'branch', 'accountName',
                       'accountNum', 'accountType', 'supplier', 'remarks')
    search_fields = ('supplier__name','accountNum')

admin.site.register(BankAccounts, BankAccountAdmin)

class PaymentDueAdmin(ROAHAdmin):
    readonly_fields = ('bankAcct', 'amount', 'dueDate',
                       'remarks', 'paid', 'frozen', 'remarks')
    search_fields = ('bankAcct__supplier__name','bankAcct__accountNum', 'dueDate', 'amount')

admin.site.register(PaymentDueDate, PaymentDueAdmin)