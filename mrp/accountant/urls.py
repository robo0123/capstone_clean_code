from django.urls import path
from . import views

urlpatterns = [
    path('', views.accountantPage, name='accountant_page'),
    path('load_bankAccts_Ajax/', views.loadBankAcctsAjax, name='loadBankAcctsAjax'),
    path('load_payments_Ajax/', views.loadPaymentsAjax, name='loadPaymentsAjax'),
    path('delete/bankacct/<int:id>/', views.deleteBankAcct, name='deleteBankAcct'),
    path('add/payment/', views.addPayment, name='addPayment'),
    path('delete/payment/<int:id>/', views.deletePayment, name='deletePayment'),
    path('toggle/paid/<int:id>/', views.togglePaid, name='togglePaid'),
    path('freeze/payment/<int:id>/', views.freeze, name='freezePayment'),
    path('rebuild/<str:formType>/', views.rebuildFormAcct, name='rebuildFormAcct'),

    
]
