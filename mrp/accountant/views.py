from django.shortcuts import render
from django.shortcuts import redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse
import json
from .forms import BankAcctForm, PaymentDueForm
from .models import BankAccounts, PaymentDueDate
from django.views.decorators.http import require_POST
from django.contrib.auth.hashers import check_password
from django.db.models import F, Sum, RestrictedError
from django.core.exceptions import ObjectDoesNotExist
from django.core import serializers
from accounts.decorators import role_required
# Create your views here.

@role_required(allowed_roles=['manager', 'accountant'])
def accountantPage(request):

    form = BankAcctForm()
    paymentForm = PaymentDueForm()
    context = {'form': form, 'paymentForm': paymentForm}

    if request.method == 'POST':
        form = BankAcctForm(request.POST)
        if form.is_valid():
            form.save()
            message = 'success'
            return JsonResponse({"form": form.as_p(), "devMessage": message})

        return JsonResponse({"form": form.as_p(), "devMessage": 'error'})

    return render(request, 'accountant/accountant.html', context)


@role_required(allowed_roles=['manager', 'accountant'])
def loadBankAcctsAjax(request):

    allBankAccts = BankAccounts.objects.all()
    jsonBankAccts = []
    for bankAcct in allBankAccts:

        sname = None
        if bankAcct.supplier is None:
            sname = ''
        else:
            sname = bankAcct.supplier.name

        aBankAcct = {'pk': bankAcct.pk,
                     'supplier': sname,
                     'bank': bankAcct.bank,
                     'accountName': bankAcct.accountName,
                     'accountNum': bankAcct.accountNum,
                     'accountType': bankAcct.accountType,
                     'branch': bankAcct.branch,
                     'remarks': bankAcct.remarks
                     }
        jsonBankAccts.append(aBankAcct)

    jsonBankAccts = json.dumps(jsonBankAccts, indent=4)

    return HttpResponse(jsonBankAccts, content_type="application/json")


@role_required(allowed_roles=['manager', 'accountant'])
@require_POST
def deleteBankAcct(request, id):

    if request.method == 'POST':
        password = request.POST.get("password", "")
        correct_password = check_password(password, request.user.password)

        if correct_password:
            bankacct = None
            message = None
            delete_success = False
            try:
                bankacct = BankAccounts.objects.get(pk=id)
                bankacct.delete()
                delete_success = True
                message = 'success'
            except RestrictedError:
                delete_success = False
                message = 'Restricted Error: A part of the system is using this Bank Account. You must delete those first.'
            except BankAccounts.DoesNotExist:
                delete_success = False
                message = 'Does Not Exist Error: BankAccount does not exist, refresh table.'

            return JsonResponse({'devMessage': message})
        else:
            return JsonResponse({'devMessage': 'Wrong Password'})


@role_required(allowed_roles=['manager', 'accountant'])
def loadPaymentsAjax(request):

    allPayments = PaymentDueDate.objects.all()
    jsonAllPayments = []
    for payment in allPayments:

        sname = None
        if payment.bankAcct.supplier is None:
            sname = ''
        else:
            sname = payment.bankAcct.supplier.name

        aPayment = {'pk': payment.pk,
                    'supplier': sname,
                    'bank': payment.bankAcct.bank,
                    'accountName': payment.bankAcct.accountName,
                    'accountNum': payment.bankAcct.accountNum,
                    'accountType': payment.bankAcct.accountType,
                    'branch': payment.bankAcct.branch,
                    'paid': payment.paid,
                    'amount': str(payment.amount),
                    'dueDate': str(payment.dueDate),
                    'remarks': payment.remarks,
                    'paid': payment.paid,
                    'frozen': payment.frozen
                    }
        jsonAllPayments.append(aPayment)

    jsonAllPayments = json.dumps(jsonAllPayments, indent=4)

    return HttpResponse(jsonAllPayments, content_type="application/json")

@role_required(allowed_roles=['manager', 'accountant'])
@require_POST
def addPayment(request):
    if request.method == 'POST':
        form = PaymentDueForm(request.POST)
        if form.is_valid():
            form.save()
            message = 'success'
            return JsonResponse({"form": form.as_p(), "devMessage": message})

        return JsonResponse({"form": form.as_p(), "devMessage": 'error'})


@role_required(allowed_roles=['manager', 'accountant'])
@require_POST
def deletePayment(request, id):

    if request.method == 'POST':
        password = request.POST.get("password", "")
        correct_password = check_password(password, request.user.password)

        if correct_password:
            payment = None
            message = None
            delete_success = False
            try:
                payment = PaymentDueDate.objects.get(pk=id)
                if payment.frozen is True:
                    message = "Error: Can't delete, already frozen."
                else:
                    payment.delete()
                    delete_success = True
                    message = 'success'
            except RestrictedError:
                delete_success = False
                message = 'Restricted Error: A part of the system is using this Payment. You must delete those first.'
            except PaymentDueDate.DoesNotExist:
                delete_success = False
                message = 'Does Not Exist Error: Payment Due Date does not exist, refresh table.'

            return JsonResponse({'devMessage': message})
        else:
            return JsonResponse({'devMessage': 'Wrong Password'})


@role_required(allowed_roles=['manager', 'accountant'])
def togglePaid(request, id):

    payment = None
    message = None
    paid = None
    try:
        payment = PaymentDueDate.objects.get(pk=id)
        if payment.frozen is True:
            message = "Error: Can't toggle pay, already frozen."
        else:
            payment.paid = not payment.paid
            payment.save()
            paid = payment.paid
            message = 'success'
    except PaymentDueDate.DoesNotExist:
        message = 'DoesNotExist Error'
    return JsonResponse({'devMessage': message, 'paid': paid})


@role_required(allowed_roles=['manager', 'accountant'])
def rebuildFormAcct(request, formType):

    if request.method == 'GET' and formType == 'payments':
        form = PaymentDueForm()
        return JsonResponse({"form": form.as_p()})


@role_required(allowed_roles=['manager', 'accountant'])
def freeze(request, id):

    payment = None
    message = None
    frozen = None
    try:
        payment = PaymentDueDate.objects.get(pk=id)
        if payment.frozen is True:
            message = 'Error: Already Frozen.'
        else:
            if payment.paid is False:
                message = 'Error: Payment has not been paid. You can delete or pay first.'
            elif payment.paid is True:
                payment.frozen = True
                payment.save()
                message = 'success'

    except PaymentDueDate.DoesNotExist:
        message = 'DoesNotExist Error'
    return JsonResponse({'devMessage': message})
