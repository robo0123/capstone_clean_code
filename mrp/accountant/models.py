from django.db import models
from simple_history.models import HistoricalRecords
from django.core.validators import MinValueValidator
from supplierDB.models import Supplier
from decimal import Decimal
import datetime

# Create your models here.


class BankAccounts(models.Model):

    bank = models.CharField(max_length=140, blank=False, null=False)
    branch = models.CharField(max_length=280, blank=True, null=True)
    accountName = models.CharField(max_length=280, blank=False, null=False)
    accountNum = models.CharField(
        max_length=140, blank=False, null=False, unique=True)

    ACCT_TYPE_CHOICES = (
        ("Savings", "Savings"),
        ("Current", "Current"),
        ("Other", "Other"),
    )
    accountType = models.CharField(max_length=32, blank=True, null=True,
                                   choices=ACCT_TYPE_CHOICES,
                                   )
    supplier = models.ForeignKey(
        Supplier, on_delete=models.RESTRICT, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)
    history = HistoricalRecords()

    def __str__(self):
        if self.supplier is None:
            return self.accountNum
        else:
            supAndAcctNum = (self.supplier.name+ ' - ' + self.accountNum)
            return supAndAcctNum


class PaymentDueDate(models.Model):
    bankAcct = models.ForeignKey(
        BankAccounts, on_delete=models.RESTRICT, blank=False, null=False)
    amount = models.DecimalField(
        decimal_places=2, max_digits=17, blank=False, null=False, validators=[MinValueValidator(Decimal('0.01'))])
    dueDate = models.DateField(
        blank=False, null=False, default=datetime.date.today)
    remarks = models.TextField(blank=True, null=True)
    paid = models.BooleanField(blank=False, null=False, default=False)
    frozen = models.BooleanField(blank=False, null=False, default=False)
    history = HistoricalRecords()

    def __str__(self):
        if self.bankAcct.supplier is None:
            return self.bankAcct.accountNum + ' - ' + str(self.dueDate) + ' - ' + str(self.amount)
        else:
            supAndAcctNum = (self.bankAcct.supplier.name+ ' - ' + self.bankAcct.accountNum + ' - ' + str(self.dueDate) + ' - ' + str(self.amount))
            return supAndAcctNum
