from django.forms import ModelForm, Textarea
from .models import BankAccounts, PaymentDueDate
from django.forms.widgets import TextInput
from django import forms
from datetime import date
import datetime

today = date.today()
minDate = datetime.datetime.strptime('19500101', '%Y%m%d').date()


class BankAcctForm(ModelForm):
    class Meta:
        model = BankAccounts
        fields = (
            'supplier', 'bank', 'accountName', 'accountNum', 'accountType', 'branch', 'remarks'
        )
        widgets = {
            'bank': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Bank'}),
            'branch': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Branch'}),
            'accountName': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Account Name'}),
            'accountNum': forms.TextInput(
                attrs={'class': "input", 'type': 'text', 'placeholder': 'Account Number'}),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),

        }

        labels = {
            'accountName': 'Account Name',
            'accountNum': 'Account Number',
            'accountType': 'Account Type',
        }


class PaymentDueForm(ModelForm):
    class Meta:
        model = PaymentDueDate
        fields = (
            'bankAcct', 'amount', 'dueDate', 'remarks'
        )

        labels = {
            'dueDate': 'Due Date',
            'bankAcct': 'Bank Account',
        }

        widgets = {
            'dueDate': forms.DateInput(format=('%Y/%m/%d'),
                                       attrs={'class': 'datepicker', 'type': 'date'
                                              }
                                       ),
            'remarks': forms.Textarea(
                attrs={'class': "textarea is-small", 'placeholder': 'Remarks', "rows": '4'}),
            'amount': forms.TextInput(
                attrs={'class': "input", 'type': 'number', 'placeholder': 'Amount', 'step': "0.01"}),
            
        }
